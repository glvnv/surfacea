#pragma once

#include <MA_Core/IObject.h>

// STL includes
#include <iostream>
#include <vector>
#include <array>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>

#include <MA_Common/StackVector.h>

using uint = unsigned int;
using uchar = unsigned char;
using namespace MA_Core;
using namespace MA;
