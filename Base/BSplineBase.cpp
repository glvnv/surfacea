#include "BSplineBase.h"

#include <MA_Common/VarOpt.h>

bool BSplineBase::calculate( double t, size_t i, const std::vector<double>& knots, size_t degree, MatrixD& n )
{
    if ( ! VERIFY( t >= knots[degree] && t <= knots[knots.size() - degree - 1] ) )
        return false;

    n.resize( degree + 1, degree + 1 );

    if ( ! VERIFY( i >= degree && i <= knots.size() - degree - 2 ) )
        return false;

    n.set( 0 );
    n.at( 0, degree ) = 1;

    // recursive N calculation
    for ( size_t r = 1; r <= degree; r++ )
    {
        for ( size_t c = degree - r; c <= degree; c++ )
        {
            const double n_forward = c == degree ? 0 : n.at( r - 1, c + 1 );
            const double n_up = n.at( r - 1, c );

            const size_t j = i - degree + c;
            const double tid1 = knots[j + r + 1];
            const double ti = knots[j];
            const double dt_forward = tid1 - knots[j + 1];
            const double dt_up = knots[j + r] - ti;

            if ( dt_up == 0 )
            {
                if ( ! VERIFY( dt_forward != 0 && c <= degree - r ) )
                    return false;

                n.at( r, c ) = ( ( ( tid1 - t ) / dt_forward ) * n_forward );
            }
            else if ( dt_forward == 0 )
            {
                if ( ! VERIFY( dt_up != 0 && c >= degree ) )
                    return false;

                n.at( r, c ) = ( ( ( t - ti ) / dt_up ) * n_up );
            }
            else
                n.at( r, c ) = ( ( ( tid1 - t ) / dt_forward ) * n_forward ) + ( ( ( t - ti ) / dt_up ) * n_up );
        }
    }

    return true;
}

int BSplineBase::findKnotIndex( double t, const std::vector<double>& knots, size_t degree )
{
    int index = -1;

    if ( ! VERIFY( t >= knots[degree] && t <= knots[knots.size() - degree - 1] ) )
        return index;

    if ( t == knots[degree] )
        index = int( degree );
    else if ( t == knots[knots.size() - degree - 1] )
        index = int( knots.size() - degree - 2 );
    else
    {
        for ( size_t i = 0; i < knots.size(); i++ )
        {
            if ( t < knots[i] )
            {
                if ( i > 0 )
                    index = int( i - 1 );
                    
                break;
            }
        }
    }

    return index;
}