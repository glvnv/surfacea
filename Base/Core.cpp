#include "StdAfx.h"
#include <MA_Core/Core.h>
#include "Creator.h"

MA_CORE_API CPICreator MA_Core::GetCreator()
{
    return Creator::Get();
}
