#pragma once

#include "StdAfx.h"
#include <MA_Common/Matrix.h>

class BSplineBase
{
public:
    static int findKnotIndex( double t, const std::vector<double>& knots, size_t degree );
    static bool calculate( double t, size_t i, const std::vector<double>& knots, size_t degree, MatrixD& n );
};

