#include "Creator.h"

#include "NurbsBuilder.h"

Creator::Creator()
{
}

const Creator* Creator::Get()
{
    static Creator instance;
    return &instance;
}

INurbsBuilderPtr Creator::nurbsBuilder() const
{
    return NurbsBuilder::Make();
}