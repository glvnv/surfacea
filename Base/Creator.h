#pragma once

#include "StdAfx.h"
#include <MA_Core/ICreator.h>

class Creator : public ICreator
{
public:
    Creator( const Creator& )         = delete;
    void operator = ( const Creator& ) = delete;
    static const Creator* Get();
    
    virtual INurbsBuilderPtr nurbsBuilder() const override;

private:    
    Creator();
};

