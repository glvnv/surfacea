#pragma once

#include <MA_Common/SmartPtr.h>

namespace MA_Core
{

MAKE_SMART_PTR_TYPES( IObject )

enum ObjectType
{
    OT_Unknown = 0,
    OT_Geometry,
    OT_Topology,
};

class IObject
{
public:
    virtual ObjectType objectType() const = 0;
};

}
