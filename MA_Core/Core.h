#pragma once

#ifdef MA_CORE_EXPORTS
    #if defined(_WIN32) && !defined(__MINGW32__)
        #define MA_CORE_API __declspec(dllexport)
    #else
        #define MA_CORE_API __attribute__ ((visibility ("default")))
    #endif
#else
    #if defined(_WIN32) && !defined(__MINGW32__)
        #define MA_CORE_API __declspec(dllimport)
        #pragma comment( lib, "MA_CORE" )
    #else
        #define MA_CORE_API __attribute__ ((visibility ("default")))
    #endif
#endif

#include <string>

namespace MA_Core
{
    class ICreator;

    MA_CORE_API const ICreator* GetCreator();
}
