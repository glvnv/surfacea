#pragma once

#include <MA_Common/SmartPtr.h>
#include <MA_Common/Matrix.h>
#include <MA_Common/Vec.h>

namespace MA_Core
{

MAKE_SMART_PTR_TYPES( INurbsBuilder )

class INurbsBuilder
{
public:
    struct Task_Curve2
    {        
        std::vector<MA::VecD2> points;
        std::array<MA::VecD2Opt, 2> bound_derivs;
        std::array<MA::VecD2Opt, 2> bound_derivs_2;
        std::vector<double> params;
        int degree = 3;
        bool periodic = false;
    };

    struct Result_Curve2
    {        
        std::vector<MA::VecD2> ctrl_points;
        std::vector<double> knots;
        int degree;
        bool periodic;
    };

    virtual bool buildCurve( const Task_Curve2& task, Result_Curve2& result ) const = 0;

    struct Task_Surface
    {
        MA::Matrix<VecD3> points;
        std::vector<MA::VecD3> du_start;
        std::vector<MA::VecD3> d2u_start;
        std::vector<MA::VecD3> du_finish;
        std::vector<MA::VecD3> d2u_finish;
        std::vector<MA::VecD3> dv_start;
        std::vector<MA::VecD3> d2v_start;
        std::vector<MA::VecD3> dv_finish;
        std::vector<MA::VecD3> d2v_finish;

        std::vector<double> u_params;
        int u_degree = 3;
        bool u_periodic = false;

        std::vector<double> v_params;
        int v_degree = 3;
        bool v_periodic = false;
    };

    struct Result_Surface
    {
        MA::Matrix<VecD3> ctrl_points;

        std::vector<double> u_knots;
        int u_degree;
        bool u_periodic;

        std::vector<double> v_knots;
        int v_degree;
        bool v_periodic;
    };

    virtual bool buildSurface( const Task_Surface& task, Result_Surface& result ) const = 0;

    virtual bool calcCurveKnots( const std::vector<double>& params, int order, bool periodic, std::vector<double>& knots ) const = 0;
};

}