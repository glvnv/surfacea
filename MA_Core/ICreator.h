#pragma once

#include "INurbsBuilder.h"

namespace MA_Core
{

class ICreator
{
public:
    virtual INurbsBuilderPtr nurbsBuilder() const = 0;
};

using CPICreator = ICreator const*;
}