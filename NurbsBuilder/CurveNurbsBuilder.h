#pragma once

#include "StdAfx.h"
#include "NurbsBuilder.h"

#include <MA_Core/INurbsBuilder.h>

class CurveNurbsBuilder
{
public:
    static bool Build( const INurbsBuilder::Task_Curve2& task, INurbsBuilder::Result_Curve2& result );
    static bool CalcKnots( const std::vector<double>& params, int degree, bool periodic, std::vector<double>& knots, DerivLevel start_deriv_level = DerivLevel::Null, DerivLevel finish_deriv_level = DerivLevel::Null );

    template<size_t N>
    static bool CalcCtrlPoints( const std::vector<VecD<N>>& points, const VecD<N>* start_deriv, const VecD<N>* start_deriv_2, const VecD<N>* end_deriv, const VecD<N>* end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<N>>& ctrl_points );

    template<size_t N>
    static bool CalcCtrlPoints_d0( const std::vector<VecD<N>>& points, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<N>>& ctrl_points );    

    template<size_t N>
    static bool CalcCtrlPoints_d1( const std::vector<VecD<N>>& points, const VecD<N>& start_deriv, const VecD<N>& end_deriv, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<N>>& ctrl_points );    

    template<size_t N>
    static bool CalcCtrlPoints_d2( const std::vector<VecD<N>>& points, const VecD<N>& start_deriv, const VecD<N>& start_deriv_2, const VecD<N>& end_deriv, const VecD<N>& end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<N>>& ctrl_points );
};