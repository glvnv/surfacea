#pragma once

#include "StdAfx.h"
#include <MA_Common/PointerBase.h>
#include <MA_Core/INurbsBuilder.h>

enum class DerivLevel
{
    Null = 0,
    First,
    Second
};

class NurbsBuilder : public INurbsBuilder, public PointerBase<NurbsBuilder>
{
public:
    MAKE_OBJECT_SMART_PTR_0 ( NurbsBuilder )

    virtual bool buildCurve( const Task_Curve2& task, Result_Curve2& result ) const override;
    virtual bool buildSurface( const Task_Surface& task, Result_Surface& result ) const override;
    virtual bool calcCurveKnots( const std::vector<double>& params, int degree, bool periodic, std::vector<double>& knots ) const override;

private:
    NurbsBuilder();
    ~NurbsBuilder();
};

MAKE_SMART_PTR_TYPES ( NurbsBuilder )