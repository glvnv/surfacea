#include "CurveNurbsBuilder.h"
#include "BSplineBase.h"

#include <MA_Common/GeomUtils.h>
#include <MA_Common/DataUtils.h>

bool CurveNurbsBuilder::Build( const INurbsBuilder::Task_Curve2& task, INurbsBuilder::Result_Curve2& result )
{
    if ( ! VERIFY( task.points.size() > 1 ) )
        return false;

    if ( ! VERIFY( task.degree > 0 && ( task.params.empty() || task.params.size() == task.points.size() ) ) )
        return false;
    
    if ( ! VERIFY( check_monotonic( task.params, true, true ) ) )
        return false;
    
    if ( ! VERIFY( ! task.periodic || task.points.front() == task.points.back() ) )
        return false;
    
    result.ctrl_points.clear();
    result.knots.clear();
    result.periodic = task.periodic;

    if ( task.bound_derivs_2[0].valid() || task.bound_derivs_2[1].valid() )
    {
        if ( ! VERIFY( task.bound_derivs_2[0].valid() 
                    && task.bound_derivs_2[1].valid() 
                    && task.bound_derivs[0].valid() 
                    && task.bound_derivs[1].valid() ) )
            return false; // NOTIMPL;

        result.degree = int( task.degree > task.points.size() + 1 ? task.points.size() + 1 : std::max( 3, task.degree ) );
    }
    else if ( task.bound_derivs[0].valid() || task.bound_derivs[1].valid() )
    {
        if ( ! VERIFY( task.bound_derivs[0].valid() && task.bound_derivs[1].valid() ) )
            return false; // NOTIMPL;

        result.degree = int( task.degree > task.points.size() ? task.points.size() : std::max( 2, task.degree ) );
    }
    else
        result.degree = int( task.degree >= task.points.size() ? task.points.size() - 1 : task.degree );

    std::vector<double> poly_knots;
    if ( task.params.empty() )
        CalcPolyParams( task.points, poly_knots );
    else
        poly_knots = task.params;

    for ( size_t i = 1; i < poly_knots.size(); ++i )
        if ( poly_knots[i-1] >= poly_knots[i] )
            return false;

    DerivLevel start_deriv_level = DerivLevel::Null;
    if ( task.bound_derivs_2[0].valid() )
        start_deriv_level = DerivLevel::Second;
    else if ( task.bound_derivs[0].valid() )
        start_deriv_level = DerivLevel::First;

    DerivLevel finish_deriv_level = DerivLevel::Null;
    if ( task.bound_derivs_2[1].valid() )
        finish_deriv_level = DerivLevel::Second;
    else if ( task.bound_derivs[1].valid() )
        finish_deriv_level = DerivLevel::First;

    if ( ! CalcKnots( poly_knots, result.degree, result.periodic, result.knots, start_deriv_level, finish_deriv_level ) )
        return false;
        
    return CalcCtrlPoints<2>( task.points, task.bound_derivs[0].get_ptr(), task.bound_derivs_2[0].get_ptr(), task.bound_derivs[1].get_ptr(), task.bound_derivs_2[1].get_ptr(), poly_knots, result.knots, result.degree, result.periodic, result.ctrl_points );
}

bool CurveNurbsBuilder::CalcKnots( const std::vector<double>& params, int degree, bool periodic, std::vector<double>& knots, DerivLevel start_deriv_level, DerivLevel finish_deriv_level )
{
    const double t_period = params.back() - params.front();
    
    //if ( periodic )
    //{
    //    knots.resize( params.size() + ( 2*degree ) );
    //    
    //    for ( int i = 0, i_end = int( knots.size() ); i < i_end; i++ )
    //    {
    //        double t = 0;
    //        for ( int j = i - degree; j < i; j++ )
    //        {
    //            double ti = 0;
    //            if ( j < 0 )
    //                ti = params[j+params.size()-1] - t_period;
    //            else if ( j >= 2 *( params.size() - 1 ) )
    //                ti = params[j+2-(2*params.size())] + 2*t_period;
    //            else if ( j >= params.size()-1 )
    //                ti = params[j+1-params.size()] + t_period;
    //            else
    //                ti = params[j];
    //
    //            t += ti;
    //        }
    //        
    //        knots[i] = t / degree;            
    //    }
    //    
    //    const double t_shift = knots[degree];
    //    for ( auto& t : knots )
    //        t -= t_shift;
    //    
    //    knots[knots.size() - degree - 1] = t_period;
    //}
    if ( periodic )
    {
        ASSERT( start_deriv_level == DerivLevel::Null && finish_deriv_level == DerivLevel::Null );

        knots.reserve( params.size() + 2*degree );
		
		for ( int i = degree; i > 0; i-- )
			knots.push_back( params[params.size() - i - 1] - t_period );
		
		for ( size_t i = 0; i < params.size(); i++ )		
			knots.push_back ( params[i] );
		
		for ( size_t i = 1; i <= degree; i++ )
			knots.push_back( params[i] + t_period );
    }
    else
    {
        knots.assign( size_t( degree + 1 ), params.front() );

        if ( start_deriv_level == DerivLevel::Null && finish_deriv_level == DerivLevel::Null )
        {
            knots.resize( params.size() + degree + 1 );

            for ( int i = degree + 1, i_end = int( knots.size() ) - degree - 1; i < i_end; i++ )
            {
                double t = 0;
                for ( int j = i - degree; j < i; j++ )
                    t += params[j];

                knots[i] = t / degree;
            }
        }
        else if ( start_deriv_level == DerivLevel::First && finish_deriv_level == DerivLevel::First )
        {
            knots.resize( params.size() + degree + 3 );

            for ( int i = degree + 1, i_end = int( knots.size() ) - degree - 1; i < i_end; i++ )
            {
                double t = 0;
                for ( int j = i - degree - 1; j < i - 1; j++ )
                    t += params[j];

                knots[i] = t / degree;
            }
        }
        else if ( start_deriv_level == DerivLevel::Second && finish_deriv_level == DerivLevel::Second )
        {
            knots.resize( params.size() + degree + 5 );
            
            for ( int i = degree + 1, i_end = int( knots.size() ) - degree - 1; i < i_end; i++ )
            {
                double t = 0;
                for ( int j = i - degree - 2; j < i - 2; j++ )
                {
                    if ( j == -1 )
                        t += ( 1.5 * params[0] ) - ( 0.5 * params[1] );
                    else if ( j == params.size() )
                        t += ( 1.5 * params.back() ) - ( 0.5 * params[params.size() - 2] );
                    else if ( VERIFY( j >= 0 && j < params.size() ) )
                        t += params[j];
                    else
                        return false;
                }
            
                knots[i] = t / degree;
            }
        }
        else
        {
            NOTIMPL;
            return false;
        }

        for ( int i = int( knots.size() ) - degree - 1; i < int( knots.size() ); i++ )
            knots[i] = t_period;
    }

    if ( ! check_monotonic( knots, true, false ) )
        return false;

    return true;
}

template<size_t N>
bool CurveNurbsBuilder::CalcCtrlPoints( const std::vector<VecD<N>>& points, const VecD<N>* start_deriv, const VecD<N>* start_deriv_2, const VecD<N>* end_deriv, const VecD<N>* end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<N>>& ctrl_points )
{
    if ( ! VERIFY( points.size() == params.size() ) )
        return false;

    if ( start_deriv && end_deriv )
    {
        if ( ! VERIFY( ! periodic ) )
            return false;

        if ( start_deriv_2 && end_deriv_2 )
            return CalcCtrlPoints_d2( points, *start_deriv, *start_deriv_2, *end_deriv, *end_deriv_2, params, knots, degree, ctrl_points );
        else
            return CalcCtrlPoints_d1( points, *start_deriv, *end_deriv, params, knots, degree, ctrl_points );
    }
    else
        return CalcCtrlPoints_d0( points, params, knots, degree, periodic, ctrl_points );
}

// explicit template instantiation
template bool CurveNurbsBuilder::CalcCtrlPoints<2>( const std::vector<VecD<2>>& points, const VecD<2>* start_deriv, const VecD<2>* start_deriv_2, const VecD<2>* end_deriv, const VecD<2>* end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<2>>& ctrl_points );
template bool CurveNurbsBuilder::CalcCtrlPoints<3>( const std::vector<VecD<3>>& points, const VecD<3>* start_deriv, const VecD<3>* start_deriv_2, const VecD<3>* end_deriv, const VecD<3>* end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<3>>& ctrl_points );

template<size_t N>
bool CurveNurbsBuilder::CalcCtrlPoints_d0( const std::vector<VecD<N>>& points, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<N>>& ctrl_points )
{
    const size_t n_points = periodic ? points.size() - 1 : points.size();

    MatrixD vector( n_points, N );
    {
        for ( size_t i = 0; i < n_points; i++ )
            vector.setRow( i, points[i] );
    }
    MatrixD matrix( n_points, n_points );

    MatrixD n;
    std::vector<size_t> mi( degree + 1 );

	for ( size_t i = 0; i < n_points; i++ )
	{
        const double t = params[i];
        const int t_index = int( BSplineBase::findKnotIndex( t, knots, size_t( degree ) ) );
        if ( t_index < 0 )
            return false;

        if ( ! BSplineBase::calculate( t, t_index, knots, size_t( degree ), n ) )
            return false;

		for ( int j = 0; j <= degree; j++ )
		{
			mi[j] = t_index - degree + j;
			if ( mi[j] >= n_points )
            {
                ASSERT( periodic );
				mi[j] -= n_points;
            }
		}

        for ( int k = 0; k <= degree; k++ )
            matrix.at( i, mi[k] ) = n.at( degree, k );
	}

    if ( ! matrix.gauss( vector ) )
        return false;

    if ( periodic )
    {
        ctrl_points.resize( n_points + degree );

        for ( size_t i = 0; i < n_points + degree; i++ )
        {
            if ( i >= n_points )
                ctrl_points[i] = ctrl_points[i-n_points];
            else
	    	    for ( size_t j = 0; j < N; j++ )
	    		    ctrl_points[i][j] = vector.at( i, j );
        }
    }
    else
    {
	    ctrl_points.resize( n_points );

	    for ( size_t i = 0; i < n_points; i++ )
	    	for ( size_t j = 0; j < N; j++ )
	    		ctrl_points[i][j] = vector.at( i, j );
    }
    
    return true;
}

// explicit template instantiation
template bool CurveNurbsBuilder::CalcCtrlPoints_d0<2>( const std::vector<VecD<2>>& points, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<2>>& ctrl_points );
template bool CurveNurbsBuilder::CalcCtrlPoints_d0<3>( const std::vector<VecD<3>>& points, const std::vector<double>& params, const std::vector<double>& knots, int degree, bool periodic, std::vector<VecD<3>>& ctrl_points );

template<size_t N>
bool CurveNurbsBuilder::CalcCtrlPoints_d1( const std::vector<VecD<N>>& points, const VecD<N>& start_deriv, const VecD<N>& end_deriv, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<N>>& ctrl_points )
{
    const size_t n_points = points.size() + 2;

    MatrixD vector( n_points, N );
    {
        vector.setRow( 0, start_deriv * ( ( knots[degree+1] - knots[1] ) / degree ) );

        for ( size_t i = 0; i < int( points.size() ); i++ )
            vector.setRow( i + 1, points[i] );

        vector.setRow( vector.rowCount() - 1, end_deriv * ( ( knots[knots.size() - 2] - knots[knots.size() - degree - 2] ) / degree ) );
    }

    MatrixD matrix( n_points, n_points );
    matrix.at( 0, 0 ) = -1; matrix.at( 0, 1 ) = 1;
    matrix.at( 1, 0 ) = 1;
    matrix.at( matrix.rowCount() - 2, matrix.columnCount() - 1 ) = 1;
    matrix.at( matrix.rowCount() - 1, matrix.columnCount() - 1 ) = 1; matrix.at( matrix.rowCount() - 1, matrix.columnCount() - 2 ) = -1;

    MatrixD n;
    std::vector<int> mi( degree + 1 );

    for ( size_t i = 2; i < n_points - 2; i++ )
    {
        const double t = params[i-1];
        const int t_index = int( BSplineBase::findKnotIndex( t, knots, size_t( degree ) ) );
        if ( t_index < 0 )
            return false;

        if ( ! BSplineBase::calculate( t, t_index, knots, size_t( degree ), n ) )
            return false;

        for ( int j = 0; j <= degree; j++ )
            mi[j] = t_index - degree + j;

        for ( int k = 0; k <= degree; k++ )
            matrix.at( i, mi[k] ) = n.at( degree, k );
    }

    if ( ! matrix.gauss( vector ) )
        return false;

    ctrl_points.resize( n_points );

    for ( size_t i = 0; i < n_points; i++ )
        for ( size_t j = 0; j < N; j++ )
            ctrl_points[i][j] = vector.at( i, j );

    return true;
}

// explicit template instantiation
template bool CurveNurbsBuilder::CalcCtrlPoints_d1<2>( const std::vector<VecD<2>>& points, const VecD<2>& start_deriv, const VecD<2>& end_deriv, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<2>>& ctrl_points );
template bool CurveNurbsBuilder::CalcCtrlPoints_d1<3>( const std::vector<VecD<3>>& points, const VecD<3>& start_deriv, const VecD<3>& end_deriv, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<3>>& ctrl_points );

template<size_t N>
bool CurveNurbsBuilder::CalcCtrlPoints_d2( const std::vector<VecD<N>>& points, const VecD<N>& start_deriv, const VecD<N>& start_deriv_2, const VecD<N>& end_deriv, const VecD<N>& end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<N>>& ctrl_points )
{
    const size_t n_points = points.size() + 4;
    
    MatrixD vector( n_points, N );
    {        
        vector.setRow( 0, start_deriv_2 * ( ( knots[degree+1] - knots[2] ) / ( ( degree ) * ( degree - 1 ) ) ) );
        vector.setRow( 1, start_deriv * ( ( knots[degree+1] - knots[1] ) / degree ) );
        
        for ( size_t i = 0; i < int( points.size() ); i++ )
            vector.setRow( i + 2, points[i] );
     
        vector.setRow( vector.rowCount() - 2, end_deriv * ( ( knots[knots.size() - 2] - knots[knots.size() - degree - 2] ) / degree ) );
        vector.setRow( vector.rowCount() - 1, end_deriv_2 * ( ( knots[knots.size() - 3] - knots[knots.size() - degree - 2] ) / ( ( degree ) * ( degree - 1 ) ) ) );
    }
        
    MatrixD matrix( n_points, n_points );
        
    matrix.at( 0, 0 ) = 1 / ( knots[degree+1] - knots[1] );
    matrix.at( 0, 1 ) = - ( knots[degree+1] - knots[1] + knots[degree+2] - knots[2] ) / ( ( knots[degree+1] - knots[1] )*( knots[degree+2] - knots[2] ) );
    matrix.at( 0, 2 ) = 1 / ( knots[degree+2] - knots[2] );

    matrix.at( 1, 0 ) = -1; matrix.at( 1, 1 ) = 1;
    matrix.at( 2, 0 ) = 1;

    matrix.at( matrix.rowCount() - 3, matrix.columnCount() - 1 ) = 1;
    matrix.at( matrix.rowCount() - 2, matrix.columnCount() - 1 ) = 1; matrix.at( matrix.rowCount() - 2, matrix.columnCount() - 2 ) = -1;

    matrix.at( matrix.rowCount() - 1, matrix.columnCount() - 1 ) = 1 / ( knots[knots.size() - 2] - knots[knots.size() - degree - 2] );
    matrix.at( matrix.rowCount() - 1, matrix.columnCount() - 2 ) = - ( knots[knots.size() - 2] - knots[knots.size() - degree - 2] + knots[knots.size() - 3] - knots[knots.size() - degree - 3] ) / ( ( knots[knots.size() - 3] - knots[knots.size() - degree - 3] )*( knots[knots.size() - 2] - knots[knots.size() - degree - 2] ) );
    matrix.at( matrix.rowCount() - 1, matrix.columnCount() - 3 ) = 1 / ( knots[knots.size() - 3] - knots[knots.size() - degree - 3] );
    
    MatrixD n;
    std::vector<int> mi( degree + 1 );
    
    for ( size_t i = 3; i < n_points - 3; i++ )
    {
        const double t = params[i-2];
        const int t_index = int( BSplineBase::findKnotIndex( t, knots, size_t( degree ) ) );
        if ( t_index < 0 )
            return false;

        if ( ! BSplineBase::calculate( t, t_index, knots, size_t( degree ), n ) )
            return false;
    
        for ( int j = 0; j <= degree; j++ )
            mi[j] = t_index - degree + j;
    
        for ( int k = 0; k <= degree; k++ )
            matrix.at( i, mi[k] ) = n.at( degree, k );
    }

    if ( ! matrix.gauss( vector ) )
        return false;
    
    ctrl_points.resize( n_points );
    
    for ( size_t i = 0; i < n_points; i++ )
        for ( size_t j = 0; j < N; j++ )
            ctrl_points[i][j] = vector.at( i, j );
    
    return true;
}

// explicit template instantiation
template bool CurveNurbsBuilder::CalcCtrlPoints_d2<2>( const std::vector<VecD<2>>& points, const VecD<2>& start_deriv, const VecD<2>& start_deriv_2, const VecD<2>& end_deriv, const VecD<2>& end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<2>>& ctrl_points );
template bool CurveNurbsBuilder::CalcCtrlPoints_d2<3>( const std::vector<VecD<3>>& points, const VecD<3>& start_deriv, const VecD<3>& start_deriv_2, const VecD<3>& end_deriv, const VecD<3>& end_deriv_2, const std::vector<double>& params, const std::vector<double>& knots, int degree, std::vector<VecD<3>>& ctrl_points );
