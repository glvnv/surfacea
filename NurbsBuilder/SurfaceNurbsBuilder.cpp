#include "SurfaceNurbsBuilder.h"
#include "CurveNurbsBuilder.h"

#include <MA_Common/DataUtils.h>
#include <MA_Common/GeomUtils.h>

bool SurfaceNurbsBuilder::Build( const INurbsBuilder::Task_Surface& task, INurbsBuilder::Result_Surface& result )
{
    if ( ! CheckTask( task ) )
        return false;
    
    result.ctrl_points.clear();

    result.u_knots.clear();
    result.u_periodic = task.u_periodic;
    
    if ( ! task.du_start.empty() || ! task.du_finish.empty() )
    {
        if ( ! VERIFY( ! task.du_start.empty() && ! task.du_finish.empty() ) )
            return false; // NOTIMPL;

        if ( ! VERIFY( task.du_start.size() == task.points.columnCount() && task.du_finish.size() == task.points.columnCount() ) )
            return false;

        if ( ! task.d2u_start.empty() || ! task.d2u_finish.empty() )
        {
            if ( ! VERIFY( ! task.d2u_start.empty() && ! task.d2u_finish.empty() ) )
                return false; // NOTIMPL;

            if ( ! VERIFY( task.d2u_start.size() == task.points.columnCount() && task.d2u_finish.size() == task.points.columnCount() ) )
                return false;

            result.u_degree = int( task.u_degree > task.points.rowCount() + 1 ? task.points.rowCount() + 1 : std::max( 3, task.u_degree ) );
        }
        else
            result.u_degree = int( task.u_degree > task.points.rowCount() ? task.points.rowCount() : std::max( 2, task.u_degree ) );
    }
    else if ( ! VERIFY( task.d2u_start.empty() && task.d2u_finish.empty() ) )
        return false;
    else
        result.u_degree = int( task.u_degree >= task.points.rowCount() ? task.points.rowCount() - 1 : task.u_degree );

    result.v_knots.clear();
    result.v_periodic = task.v_periodic;

    if ( ! task.dv_start.empty() || ! task.dv_finish.empty() )
    {
        if ( ! VERIFY( ! task.dv_start.empty() && ! task.dv_finish.empty() ) )
            return false; // NOTIMPL;

        if ( ! VERIFY( task.dv_start.size() == task.points.rowCount() && task.dv_finish.size() == task.points.rowCount() ) )
            return false;

        if ( ! task.d2v_start.empty() || ! task.d2v_finish.empty() )
        {
            if ( ! VERIFY( ! task.d2v_start.empty() && ! task.d2v_finish.empty() ) )
                return false; // NOTIMPL;

            if ( ! VERIFY( task.d2v_start.size() == task.points.rowCount() && task.d2v_finish.size() == task.points.rowCount() ) )
                return false;

            result.v_degree = int( task.v_degree > task.points.rowCount() + 1 ? task.points.rowCount() + 1 : std::max( 3, task.v_degree ) );
        }
        else
            result.v_degree = int( task.v_degree > task.points.rowCount() ? task.points.rowCount() : std::max( 2, task.v_degree ) );
    }
    else if ( ! VERIFY( task.d2v_start.empty() && task.d2v_finish.empty() ) )
        return false;
    else
        result.v_degree = int( task.v_degree >= task.points.rowCount() ? task.points.rowCount() - 1 : task.v_degree );

    std::vector<double> u_poly_knots;
    if ( task.u_params.empty() )
        CalcPolyParams( task.points, 0, u_poly_knots );
    else
        u_poly_knots = task.u_params;
    
    std::vector<double> v_poly_knots;
    if ( task.v_params.empty() )
        CalcPolyParams( task.points, 1, v_poly_knots );
    else
        v_poly_knots = task.v_params;
    
    if ( ! VERIFY( u_poly_knots.front() != u_poly_knots.back() && v_poly_knots.front() != v_poly_knots.back() ) )
        return false;

    DerivLevel u_start_deriv_level = DerivLevel::Null;
    if ( ! task.du_start.empty() )
    {
        if ( task.d2u_start.empty() )
            u_start_deriv_level = DerivLevel::First;
        else
            u_start_deriv_level = DerivLevel::Second;
    }

    DerivLevel u_finish_deriv_level = DerivLevel::Null;
    if ( ! task.du_finish.empty() )
    {
        if ( task.d2u_finish.empty() )
            u_finish_deriv_level = DerivLevel::First;
        else
            u_finish_deriv_level = DerivLevel::Second;
    }

    if ( ! CurveNurbsBuilder::CalcKnots( u_poly_knots, result.u_degree, result.u_periodic, result.u_knots, u_start_deriv_level, u_finish_deriv_level ) )
        return false;

    DerivLevel v_start_deriv_level = DerivLevel::Null;
    if ( ! task.dv_start.empty() )
    {
        if ( task.d2v_start.empty() )
            v_start_deriv_level = DerivLevel::First;
        else
            v_start_deriv_level = DerivLevel::Second;
    }

    DerivLevel v_finish_deriv_level = DerivLevel::Null;
    if ( ! task.dv_finish.empty() )
    {
        if ( task.d2v_finish.empty() )
            v_finish_deriv_level = DerivLevel::First;
        else
            v_finish_deriv_level = DerivLevel::Second;
    }

    if ( ! CurveNurbsBuilder::CalcKnots( v_poly_knots, result.v_degree, result.v_periodic, result.v_knots, v_start_deriv_level, v_finish_deriv_level ) )
        return false;

    return CalcCtrlPoints( task.points
                         , task.du_start, task.du_finish, task.dv_start, task.dv_finish
                         , task.d2u_start, task.d2u_finish, task.d2v_start, task.d2v_finish
                         , u_poly_knots, result.u_knots, result.u_degree, result.u_periodic
                         , v_poly_knots, result.v_knots, result.v_degree, result.v_periodic
                         , result.ctrl_points );
}

bool SurfaceNurbsBuilder::CheckTask( const INurbsBuilder::Task_Surface& task )
{
    if ( ! VERIFY( task.points.columnCount() > 1 && task.points.rowCount() > 1 ) )
        return false;
    
    if ( ! VERIFY( task.u_degree > 0 && ( task.u_params.empty() || task.u_params.size() == task.points.rowCount() ) ) )
        return false;

    if ( ! VERIFY( task.v_degree > 0 && ( task.v_params.empty() || task.v_params.size() == task.points.columnCount() ) ) )
        return false;
    
    if ( ! VERIFY( check_monotonic( task.u_params, true, true ) && check_monotonic( task.v_params, true, true ) ) )
        return false;
    
    if ( task.u_periodic )
    {
        for ( size_t i = 0; i < task.points.columnCount(); ++i )
            if ( ! VERIFY( task.points.at( 0, i ) == task.points.at( task.points.rowCount() - 1, i ) ) )
                return false;
    }

    if ( task.v_periodic )
    {
        for ( size_t i = 0; i < task.points.rowCount(); ++i )
            if ( ! VERIFY( task.points.at( i, 0 ) == task.points.at( i, task.points.columnCount() - 1 ) ) )
                return false;
    }

    return true;
}

void SurfaceNurbsBuilder::CalcPolyParams( const Matrix<VecD3>& points, int axis, std::vector<double>& params )
{
    params.clear();

    if ( axis % 2 == 0 )
    {
        std::vector<VecD3> column_points;
        params.resize( points.rowCount() );
        for ( size_t i = 0; i < points.columnCount(); ++i )
        {
            points.extractColumn( i, column_points );
            std::vector<double> column_params;
            ::CalcPolyParams( column_points, column_params );

            for ( size_t j = 0; j < points.rowCount(); ++j )
                params[j] += column_params[j];
        }
        
        for ( auto& p : params )
            p /= points.columnCount();
    }
    else
    {
        std::vector<VecD3> row_points;
        params.resize( points.columnCount() );
        for ( size_t i = 0; i < points.rowCount(); ++i )
        {
            points.extractRow( i, row_points );
            std::vector<double> row_params;
            ::CalcPolyParams( row_points, row_params );
        
            for ( size_t j = 0; j < points.columnCount(); ++j )
                params[j] += row_params[j];
        }
        
        for ( auto& p : params )
            p /= points.rowCount();
    }
}

bool SurfaceNurbsBuilder::CalcCtrlPoints( const Matrix<VecD3>& points
                                        , const std::vector<VecD3>& du_start
                                        , const std::vector<VecD3>& du_finish
                                        , const std::vector<VecD3>& dv_start
                                        , const std::vector<VecD3>& dv_finish
                                        , const std::vector<VecD3>& d2u_start
                                        , const std::vector<VecD3>& d2u_finish
                                        , const std::vector<VecD3>& d2v_start
                                        , const std::vector<VecD3>& d2v_finish
                                        , const std::vector<double>& u_params
                                        , const std::vector<double>& u_knots
                                        , int u_degree
                                        , bool u_periodic
                                        , const std::vector<double>& v_params
                                        , const std::vector<double>& v_knots
                                        , int v_degree
                                        , bool v_periodic
                                        , Matrix<VecD3>& ctrl_points )
{
    if ( ! d2u_start.empty() && ! d2u_finish.empty() && ! d2v_start.empty() && ! d2v_finish.empty() )
    {
        if ( ! VERIFY( ! u_periodic && ! v_periodic ) )
            return false;

        return CalcCtrlPoints_d2u_d2v( points, du_start, du_finish, dv_start, dv_finish, d2u_start, d2u_finish, d2v_start, d2v_finish, u_params, u_knots, u_degree, v_params, v_knots, v_degree, ctrl_points );        
    }
    else if ( ! d2u_start.empty() || ! d2u_finish.empty() || ! d2v_start.empty() || ! d2v_finish.empty() )
    {
        NOTIMPL;
        return false;
    }
    else if ( du_start.empty() && du_finish.empty() && dv_start.empty() && dv_finish.empty() )
        return CalcCtrlPoints_d0( points, u_params, u_knots, u_degree, u_periodic, v_params, v_knots, v_degree, v_periodic, ctrl_points );
    else if ( ! du_start.empty() && ! du_finish.empty() && dv_start.empty() && dv_finish.empty() )
    {
        if ( ! VERIFY( ! u_periodic ) )
            return false;

        return CalcCtrlPoints_du( points, du_start, du_finish, u_params, u_knots, u_degree, v_params, v_knots, v_degree, v_periodic, ctrl_points );
    }
    else if ( du_start.empty() && du_finish.empty() && ! dv_start.empty() && ! dv_finish.empty() )
    {
        if ( ! VERIFY( ! v_periodic ) )
            return false;

        return CalcCtrlPoints_dv( points, dv_start, dv_finish, u_params, u_knots, u_degree, u_periodic, v_params, v_knots, v_degree, ctrl_points );
    }    
    else if ( ! du_start.empty() && ! du_finish.empty() && ! dv_start.empty() && ! dv_finish.empty() )
    {
        if ( ! VERIFY( ! u_periodic && ! v_periodic ) )
            return false;

        return CalcCtrlPoints_du_dv( points, du_start, du_finish, dv_start, dv_finish, u_params, u_knots, u_degree, v_params, v_knots, v_degree, ctrl_points );
    }
    else
    {
        NOTIMPL;
        return false;
    }
}

bool SurfaceNurbsBuilder::CalcCtrlPoints_d0( const Matrix<VecD3>& points
                                           , const std::vector<double>& u_params
                                           , const std::vector<double>& u_knots
                                           , int u_degree
                                           , bool u_periodic
                                           , const std::vector<double>& v_params
                                           , const std::vector<double>& v_knots
                                           , int v_degree
                                           , bool v_periodic
                                           , Matrix<VecD3>& ctrl_points )
{
    Matrix<VecD3> r_points( points.rowCount(), points.columnCount() );
    std::vector<VecD3> r_u_line( points.rowCount() );
    std::vector<VecD3> u_column( points.rowCount() );

    for ( size_t iv = 0; iv < points.columnCount(); ++iv )
    {
        points.extractColumn( iv, u_column );

        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( u_column, nullptr, nullptr, nullptr, nullptr, u_params, u_knots, u_degree, u_periodic, r_u_line ) )
            return false;

        r_points.setColumn( iv, r_u_line.data() );
    }

    ctrl_points.resize( points.rowCount(), points.columnCount() );
    std::vector<VecD3> r_v_line( points.columnCount() );
    std::vector<VecD3> v_row( points.columnCount() );

    for ( size_t iu = 0; iu < r_points.rowCount(); ++iu )
    {
        r_points.extractRow( iu, v_row );

        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( v_row, nullptr, nullptr, nullptr, nullptr, v_params, v_knots, v_degree, v_periodic, r_v_line ) )
            return false;

        ctrl_points.setRow( iu, r_v_line.data() );
    }

    return true;
}

bool SurfaceNurbsBuilder::CalcCtrlPoints_du( const Matrix<VecD3>& points
                                           , const std::vector<VecD3>& du_start
                                           , const std::vector<VecD3>& du_finish
                                           , const std::vector<double>& u_params
                                           , const std::vector<double>& u_knots
                                           , int u_degree
                                           , const std::vector<double>& v_params
                                           , const std::vector<double>& v_knots
                                           , int v_degree
                                           , bool v_periodic
                                           , Matrix<VecD3>& ctrl_points )
{
    const size_t u_n_points = points.rowCount() + 2;

    Matrix<VecD3> r_points( u_n_points, points.columnCount() );
    std::vector<VecD3> r_u_line( u_n_points );
    std::vector<VecD3> u_column( points.rowCount() );

    for ( size_t iv = 0; iv < points.columnCount(); ++iv )
    {
        points.extractColumn( iv, u_column );
        
        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( u_column, &du_start[iv], nullptr, &du_finish[iv], nullptr, u_params, u_knots, u_degree, false, r_u_line ) )
            return false;

        r_points.setColumn( iv, r_u_line.data() );
    }

    ctrl_points.resize( u_n_points, points.columnCount() );
    std::vector<VecD3> r_v_line( points.columnCount() );
    std::vector<VecD3> v_row( points.columnCount() );

    for ( size_t iu = 0; iu < r_points.rowCount(); ++iu )
    {
        r_points.extractRow( iu, v_row );

        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( v_row, nullptr, nullptr, nullptr, nullptr, v_params, v_knots, v_degree, v_periodic, r_v_line ) )
            return false;

        ctrl_points.setRow( iu, r_v_line.data() );
    }

    return true;
}

bool SurfaceNurbsBuilder::CalcCtrlPoints_dv( const Matrix<VecD3>& points
                                           , const std::vector<VecD3>& dv_start
                                           , const std::vector<VecD3>& dv_finish
                                           , const std::vector<double>& u_params
                                           , const std::vector<double>& u_knots
                                           , int u_degree
                                           , bool u_periodic
                                           , const std::vector<double>& v_params
                                           , const std::vector<double>& v_knots
                                           , int v_degree                              
                                           , Matrix<VecD3>& ctrl_points )
{
    const size_t v_n_points = points.columnCount() + 2;

    Matrix<VecD3> r_points( points.rowCount(), v_n_points );
    std::vector<VecD3> r_u_line( points.rowCount() );
    std::vector<VecD3> u_column( points.rowCount() );

    for ( size_t iv = 0; iv < v_n_points; ++iv )
    {
        if ( iv == 0 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( dv_start, nullptr, nullptr, nullptr, nullptr, u_params, u_knots, u_degree, u_periodic, r_u_line ) )
                return false;
        }
        else if ( iv == v_n_points - 1 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( dv_finish, nullptr, nullptr, nullptr, nullptr, u_params, u_knots, u_degree, u_periodic, r_u_line ) )
                return false;
        }
        else
        {
            points.extractColumn( iv - 1, u_column );
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( u_column, nullptr, nullptr, nullptr, nullptr, u_params, u_knots, u_degree, u_periodic, r_u_line ) )
                return false;
        }
    
        r_points.setColumn( iv, r_u_line.data() );
    }

    ctrl_points.resize( points.rowCount(), v_n_points );
    std::vector<VecD3> r_v_line( v_n_points );
    std::vector<VecD3> v_row( points.columnCount() );
    std::vector<VecD3> rv_row( v_n_points );
    
    for ( size_t iu = 0; iu < r_points.rowCount(); ++iu )
    {
        r_points.extractRow( iu, rv_row );
        v_row.assign( rv_row.begin() + 1, rv_row.end() - 1 );
    
        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( v_row, &rv_row.front(), nullptr, &rv_row.back(), nullptr, v_params, v_knots, v_degree, false, r_v_line ) )
            return false;
    
        ctrl_points.setRow( iu, r_v_line.data() );
    }

    return true;
}

bool SurfaceNurbsBuilder::CalcCtrlPoints_du_dv( const Matrix<VecD3>& points
                                              , const std::vector<VecD3>& du_start
                                              , const std::vector<VecD3>& du_finish
                                              , const std::vector<VecD3>& dv_start
                                              , const std::vector<VecD3>& dv_finish
                                              , const std::vector<double>& u_params
                                              , const std::vector<double>& u_knots
                                              , int u_degree
                                              , const std::vector<double>& v_params
                                              , const std::vector<double>& v_knots
                                              , int v_degree
                                              , Matrix<VecD3>& ctrl_points )
{
    const size_t u_n_points = points.rowCount() + 2;
    const size_t v_n_points = points.columnCount() + 2;

    Matrix<VecD3> r_points( u_n_points, v_n_points );
    std::vector<VecD3> r_u_line( u_n_points );
    std::vector<VecD3> u_column( points.rowCount() );
    const VecD3 corner_dudv( 0, 0, 0 );

    for ( size_t iv = 0; iv < v_n_points; ++iv )
    {
        if ( iv == 0 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( dv_start, &corner_dudv, nullptr, &corner_dudv, nullptr, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
        else if ( iv == v_n_points - 1 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( dv_finish, &corner_dudv, nullptr, &corner_dudv, nullptr, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
        else
        {
            points.extractColumn( iv - 1, u_column );
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( u_column, &du_start[iv-1], nullptr, &du_finish[iv-1], nullptr, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }

        r_points.setColumn( iv, r_u_line.data() );
    }
    
    ctrl_points.resize( u_n_points, v_n_points );
    std::vector<VecD3> r_v_line( v_n_points );
    std::vector<VecD3> v_row( points.columnCount() );
    std::vector<VecD3> rv_row( v_n_points );
    
    for ( size_t iu = 0; iu < r_points.rowCount(); ++iu )
    {
        r_points.extractRow( iu, rv_row );
        v_row.assign( rv_row.begin() + 1, rv_row.end() - 1 );
    
        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( v_row, &rv_row.front(), nullptr, &rv_row.back(), nullptr, v_params, v_knots, v_degree, false, r_v_line ) )
            return false;
    
        ctrl_points.setRow( iu, r_v_line.data() );
    }

    return true;
}

bool SurfaceNurbsBuilder::CalcCtrlPoints_d2u_d2v( const Matrix<VecD3>& points
                                                , const std::vector<VecD3>& du_start
                                                , const std::vector<VecD3>& du_finish
                                                , const std::vector<VecD3>& dv_start
                                                , const std::vector<VecD3>& dv_finish
                                                , const std::vector<VecD3>& d2u_start
                                                , const std::vector<VecD3>& d2u_finish
                                                , const std::vector<VecD3>& d2v_start
                                                , const std::vector<VecD3>& d2v_finish
                                                , const std::vector<double>& u_params
                                                , const std::vector<double>& u_knots
                                                , int u_degree
                                                , const std::vector<double>& v_params
                                                , const std::vector<double>& v_knots
                                                , int v_degree
                                                , Matrix<VecD3>& ctrl_points )
{
    const size_t u_n_points = points.rowCount() + 4;
    const size_t v_n_points = points.columnCount() + 4;
    
    Matrix<VecD3> r_points( u_n_points, v_n_points );
    std::vector<VecD3> r_u_line( u_n_points );
    std::vector<VecD3> u_column( points.rowCount() );
    const VecD3 corner_deriv( 0, 0, 0 );
    
    for ( size_t iv = 0; iv < v_n_points; ++iv )
    {
        if ( iv == 0 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( d2v_start, &corner_deriv, &corner_deriv, &corner_deriv, &corner_deriv, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
        else if ( iv == 1 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( dv_start, &corner_deriv, &corner_deriv, &corner_deriv, &corner_deriv, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
        else if ( iv == v_n_points - 2 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( dv_finish, &corner_deriv, &corner_deriv, &corner_deriv, &corner_deriv, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
        else if ( iv == v_n_points - 1 )
        {
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( d2v_finish, &corner_deriv, &corner_deriv, &corner_deriv, &corner_deriv, u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
        else
        {
            points.extractColumn( iv - 2, u_column );
            if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( u_column, &du_start[iv-2], &d2u_start[iv-2], &du_finish[iv-2], &d2u_finish[iv-2], u_params, u_knots, u_degree, false, r_u_line ) )
                return false;
        }
    
        r_points.setColumn( iv, r_u_line.data() );
    }
    
    ctrl_points.resize( u_n_points, v_n_points );
    std::vector<VecD3> r_v_line( v_n_points );
    std::vector<VecD3> v_row( points.columnCount() );
    std::vector<VecD3> rv_row( v_n_points );
    
    for ( size_t iu = 0; iu < r_points.rowCount(); ++iu )
    {
        r_points.extractRow( iu, rv_row );
        v_row.assign( rv_row.begin() + 2, rv_row.end() - 2 );
    
        if ( ! CurveNurbsBuilder::CalcCtrlPoints<3>( v_row, &rv_row[1], &rv_row[0], &rv_row[rv_row.size()-2], &rv_row[rv_row.size()-1], v_params, v_knots, v_degree, false, r_v_line ) )
            return false;
    
        ctrl_points.setRow( iu, r_v_line.data() );
    }
    
    return true;
}