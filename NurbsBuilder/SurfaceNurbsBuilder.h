#pragma once

#include "StdAfx.h"

#include <MA_Core/INurbsBuilder.h>

class SurfaceNurbsBuilder
{
public:
    static bool Build( const INurbsBuilder::Task_Surface& task, INurbsBuilder::Result_Surface& result );

private:
    static bool CheckTask( const INurbsBuilder::Task_Surface& task );
    static void CalcPolyParams( const Matrix<VecD3>& points, int axis, std::vector<double>& params );

    static bool CalcCtrlPoints( const Matrix<VecD3>& points
                              , const std::vector<VecD3>& du_start
                              , const std::vector<VecD3>& du_finish
                              , const std::vector<VecD3>& dv_start
                              , const std::vector<VecD3>& dv_finish
                              , const std::vector<VecD3>& d2u_start
                              , const std::vector<VecD3>& d2u_finish
                              , const std::vector<VecD3>& d2v_start
                              , const std::vector<VecD3>& d2v_finish
                              , const std::vector<double>& u_params
                              , const std::vector<double>& u_knots
                              , int u_degree
                              , bool u_periodic
                              , const std::vector<double>& v_params
                              , const std::vector<double>& v_knots
                              , int v_degree
                              , bool v_periodic
                              , Matrix<VecD3>& ctrl_points );

    static bool CalcCtrlPoints_d0( const Matrix<VecD3>& points
                              , const std::vector<double>& u_params
                              , const std::vector<double>& u_knots
                              , int u_degree
                              , bool u_periodic
                              , const std::vector<double>& v_params
                              , const std::vector<double>& v_knots
                              , int v_degree
                              , bool v_periodic
                              , Matrix<VecD3>& ctrl_points );

    static bool CalcCtrlPoints_du( const Matrix<VecD3>& points
                              , const std::vector<VecD3>& du_start
                              , const std::vector<VecD3>& du_finish
                              , const std::vector<double>& u_params
                              , const std::vector<double>& u_knots
                              , int u_degree
                              , const std::vector<double>& v_params
                              , const std::vector<double>& v_knots
                              , int v_degree
                              , bool v_periodic
                              , Matrix<VecD3>& ctrl_points );

    static bool CalcCtrlPoints_dv( const Matrix<VecD3>& points
                              , const std::vector<VecD3>& dv_start
                              , const std::vector<VecD3>& dv_finish
                              , const std::vector<double>& u_params
                              , const std::vector<double>& u_knots
                              , int u_degree
                              , bool u_periodic
                              , const std::vector<double>& v_params
                              , const std::vector<double>& v_knots
                              , int v_degree                              
                              , Matrix<VecD3>& ctrl_points );

    static bool CalcCtrlPoints_du_dv( const Matrix<VecD3>& points
                                    , const std::vector<VecD3>& du_start
                                    , const std::vector<VecD3>& du_finish
                                    , const std::vector<VecD3>& dv_start
                                    , const std::vector<VecD3>& dv_finish
                                    , const std::vector<double>& u_params
                                    , const std::vector<double>& u_knots
                                    , int u_degree
                                    , const std::vector<double>& v_params
                                    , const std::vector<double>& v_knots
                                    , int v_degree
                                    , Matrix<VecD3>& ctrl_points );

    static bool CalcCtrlPoints_d2u_d2v( const Matrix<VecD3>& points
                                      , const std::vector<VecD3>& du_start
                                      , const std::vector<VecD3>& du_finish
                                      , const std::vector<VecD3>& dv_start
                                      , const std::vector<VecD3>& dv_finish
                                      , const std::vector<VecD3>& d2u_start
                                      , const std::vector<VecD3>& d2u_finish
                                      , const std::vector<VecD3>& d2v_start
                                      , const std::vector<VecD3>& d2v_finish
                                      , const std::vector<double>& u_params
                                      , const std::vector<double>& u_knots
                                      , int u_degree
                                      , const std::vector<double>& v_params
                                      , const std::vector<double>& v_knots
                                      , int v_degree
                                      , Matrix<VecD3>& ctrl_points );
};