#include "NurbsBuilder.h"
#include "CurveNurbsBuilder.h"
#include "SurfaceNurbsBuilder.h"

NurbsBuilder::NurbsBuilder()
{
}

NurbsBuilder::~NurbsBuilder()
{
}

bool NurbsBuilder::buildCurve( const Task_Curve2& task, Result_Curve2& result ) const
{
    return CurveNurbsBuilder::Build( task, result );
}

bool NurbsBuilder::buildSurface( const Task_Surface& task, Result_Surface& result ) const
{
    return SurfaceNurbsBuilder::Build( task, result );
}

bool NurbsBuilder::calcCurveKnots( const std::vector<double>& params, int degree, bool periodic, std::vector<double>& knots ) const
{
    return CurveNurbsBuilder::CalcKnots( params, degree, periodic, knots );
}

