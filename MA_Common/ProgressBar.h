#pragma once

#include "String.h"
#include <functional>

namespace MA
{

class ProgressBar
{
public:
    ProgressBar( const std::function<void( bool&, double, const String& )>& set_fn ) : m_set_fn( set_fn ){}
    void set( bool& stop, double progress_from_0_to_1 = -1, const String& status = String() ) const { m_set_fn( stop, progress_from_0_to_1, status ); }

private:
    std::function<void( bool&, double, const String& )> m_set_fn;
};

}

