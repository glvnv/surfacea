#pragma once

#include <memory>

namespace MA
{

#define MAKE_PTR_TYPES( Type )       \
class Type;                          \
using P##Type = Type*;               \
using CP##Type = Type const*;        \
using R##Type = Type&;               \
using CR##Type = Type const&;        \
using CPC##Type = Type const* const; 

#define MAKE_SHARED_PTR_TYPES( Type )                 \
using Type ## Ptr      = std::shared_ptr<Type>;       \
using Type ## CPtr     = std::shared_ptr<const Type>;

//#define MAKE_UNIQUE_PTR_TYPES( Type )                 \
//using Type ## PtrU     = std::unique_ptr<Type>;       \
//using Type ## CPtrU    = std::unique_ptr<const Type>;
//
//#define MAKE_WEAK_PTR_TYPES( Type )                 \
//using Type ## PtrW     = std::weak_ptr<Type>;       \
//using Type ## CPtrW    = std::weak_ptr<const Type>;

#define MAKE_SMART_PTR_TYPES( Type )                  \
MAKE_PTR_TYPES( Type )                                \
MAKE_SHARED_PTR_TYPES( Type )                         \
//MAKE_UNIQUE_PTR_TYPES( Type )                         \
//MAKE_WEAK_PTR_TYPES( Type )

}
