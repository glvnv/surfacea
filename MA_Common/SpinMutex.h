#pragma once

#include <atomic>
#include <thread>

namespace MA
{

class SpinMutex
{
public:
    inline void lock() { while( m_flag.test_and_set( std::memory_order_acquire ) ); }
    inline void unlock() { m_flag.clear( std::memory_order_release ); }

private:
    std::atomic_flag m_flag = ATOMIC_FLAG_INIT;
};

class SpinMutexGuard
{
public:
    SpinMutexGuard( SpinMutex& lock ) : m_lock( lock ) { m_lock.lock(); }
    ~SpinMutexGuard() { m_lock.unlock(); }

private:
    SpinMutex& m_lock;
};

}

