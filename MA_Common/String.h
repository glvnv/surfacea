#pragma once

#include <string>
#include <assert.h>

const size_t MAX_STR_LENGTH = 1048576; // 2^20

namespace MA
{

using Char = char16_t;

class String : public std::u16string
{
public:
    String() = default;

    String( const String& str ) = default;

    String( const std::u16string& str )
        : std::u16string( str )
    {
    }

    String( const char* data )
    {
        set( data );
    }

    std::string to_std_string() const
    {
        std::string std_str;
        to_std_string( std_str );
        return std_str;
    }

    template<typename S>
    void from_string( const S& str )
    {
        reserve( str.size() );

        for ( auto i = str.begin(); i != str.end(); i++ )
            push_back( Char( *i ) );
    }

    void to_std_string( std::string& std_str ) const
    {
        std_str.reserve( size() );

        for ( auto i = begin(); i != end(); i++ )
            std_str.push_back( char( *i ) );
    }

    void to_std_wstring( std::wstring& std_wstr ) const
    {
        std_wstr.reserve( size() );

        for ( auto i = begin(); i != end(); i++ )
            std_wstr.push_back( wchar_t( *i ) );
    }

    void set( const char* data )
    {
        clear();

        for ( size_t i = 0; i < MAX_STR_LENGTH; i++ )
        {
            assert( i < MAX_STR_LENGTH - 1 );
            if ( data[i] == 0 )
                break;

            push_back( Char( data[i] ) );
        }
    }
};

}
