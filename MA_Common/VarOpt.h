#pragma once

#include "Exception.h"
#include <functional>
#include <vector>

namespace MA
{

template<typename T>
class VarOpt
{
public:    
    typedef std::function<void()> fnRelease;

    explicit VarOpt( const T& var ) : m_var( var ), m_valid( true ) {}
    explicit VarOpt() : m_var(), m_valid( false ) {}
    void reset()
    {
        m_var   = T();
        m_valid = false;
    }

    inline bool valid() const { return m_valid; }

    inline const T& get() const
    {
        if ( ! m_valid )
            throw Exception( Exception::S_NotInitialized, "VarOpt is not valid" );

        return m_var;
    }

    inline const T* get_ptr() const
    {
        return m_valid ? & m_var : nullptr;
    }

    inline const T& get_if_valid( const T& default_value ) const
    {
        if ( ! m_valid )
            return default_value;

        return m_var;
    }

    void set( const T& v )
    {
        m_var = v;
        m_valid = true;
    }
    
    VarOpt& operator = ( const VarOpt& vv )
    {        
        m_var = vv.m_var;
        m_valid = vv.m_valid;
        return *this;
    }

    VarOpt& operator = ( const T& v )
    {
        m_var = v;
        m_valid = true;
        return *this;
    }

    bool operator == ( const VarOpt& vv ) const
    {
        if ( vv.m_valid == m_valid )
        {
            if ( ! m_valid )
                throw Exception( Exception::S_NotInitialized, "VarOpt is not valid" );

            return vv.m_var == m_var;
        }
        else
            return false;
    }

    bool operator == ( const T& v ) const
    {
        if ( m_valid )
            return m_var == v;
        else
            throw Exception( Exception::S_NotInitialized, "VarOpt is not valid" );
    }

    operator const T& () const
    {
        if ( ! m_valid )
            throw Exception( Exception::S_NotInitialized, "VarOpt is not valid" );

        return m_var;
    }

private:
    T         m_var;
    bool      m_valid;
};

typedef VarOpt<size_t>       SizeTOpt;
typedef VarOpt<int>          IntOpt;
typedef VarOpt<unsigned int> UIntOpt;
typedef VarOpt<bool>         BoolOpt;
typedef VarOpt<char>         CharOpt;
typedef VarOpt<double>       DoubleOpt;

}
