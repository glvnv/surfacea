#pragma once

#include <memory>

namespace MA
{

template <class Impl>
class PointerBase //: public std::enable_shared_from_this<Impl>
{
public:    
#if !defined(_WIN32) || defined(__MINGW32__)
    virtual ~PointerBase() {}
#endif

    static void Release( Impl* p )
    {
        if ( p )
            delete p;
    }
};

}

#define MAKE_OBJECT_SMART_PTR_0( Type )    								       \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make()    		                           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type(), &MA::PointerBase<Type>::Release );   \
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_1LCR( Type, Arg )   								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( const Arg& a )					           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( a ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_2LCR( Type, Arg0, Arg1 )    						   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( const Arg0& a0, const Arg1& a1 )          \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( a0, a1 ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;                                           

#define MAKE_OBJECT_SMART_PTR_1P( Type, Arg )    								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( Arg* a )	         				           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( a ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_2P( Type, Arg0, Arg1 )    								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( Arg0* a0, Arg1* a1 )   				           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( a0, a1 ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_1CP( Type, Arg )    								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( const Arg* a )	         				   \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( a ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_2CP( Type, Arg0, Arg1 )    								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( const Arg0* a0, const Arg1* a1 )	           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( a0, a1 ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_1RR( Type, Arg )   								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( Arg && a )					           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( std::move( a ) ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

#define MAKE_OBJECT_SMART_PTR_2RR( Type, Arg0, Arg1 )   								   \
    static std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )> Make( Arg0 && a0, Arg1 && a1 )					           \
    {                                                                              \
        return std::unique_ptr<Type, decltype( &MA::PointerBase<Type>::Release )>( new Type( std::move( a0 ), std::move( a1 ) ), &MA::PointerBase<Type>::Release );\
    }	                                                                           \
    friend class MA::PointerBase<Type>;

/*#define CREATE_DEFAULT_0( Type ) \
    static std::shared_ptr<Type> create() \
    { \
        return make(); \
    }

#define CREATE_DEFAULT_1( Type, Arg ) \
    static std::shared_ptr<Type> create( const Arg& a ) \
    { \
        return make( a ); \
    }

#define CREATE_DEFAULT_2( Type, Arg0, Arg1 ) \
    static std::shared_ptr<Type> create( const Arg0& a0, const Arg1& a1 ) \
    { \
        return make( a0, a1 ); \
    }*/
