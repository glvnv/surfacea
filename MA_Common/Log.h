#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "Assert.h"
#include "SpinMutex.h"

namespace MA
{

const std::string log_filepath = "log.txt";
static SpinMutex log_mutex;

inline void LOG_CLEAR()
{
    SpinMutexGuard log_guard( log_mutex );
    
    std::ofstream new_log( log_filepath, std::ios::binary );
    if ( ! new_log )
        return;
    
    std::string s;
    new_log.write( s.data(), s.size() );
    new_log.close();
}

inline void LOG( const std::string& text )
{
    std::string curr_text;

    SpinMutexGuard log_guard( log_mutex );
    std::ifstream curr_log_file( log_filepath, std::ios::binary );
    if ( curr_log_file.is_open() )
    {
        curr_text.assign( ( std::istreambuf_iterator<char>( curr_log_file ) ), std::istreambuf_iterator<char>() );
        curr_log_file.close();
    }
    
    if ( ! curr_text.empty() )
        curr_text.push_back( '\n' );
    
    curr_text.insert( curr_text.end(), text.begin(), text.end() );

    std::ofstream new_log( log_filepath, std::ios::binary );
    if ( ! VERIFY( new_log ) )
        return;
    
    new_log.write( curr_text.data(), curr_text.size() );
    new_log.close();
}

inline void LOG( const std::string& text, double value )
{
    std::ostringstream ss;
    ss << text;
    ss << " ";
    ss << value;
    LOG( ss.str() );
}

}
