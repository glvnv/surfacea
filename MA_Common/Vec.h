#pragma once

#include <cmath>
#include <stdarg.h>

#include "VarOpt.h"
//#define _USE_MATH_DEFINES
//#include <math.h>
#include <float.h>
//#include <stdio.h>

namespace MA
{

template<typename T, size_t N> class Vec;

template <typename T>
Vec<T, 3> Vec3Init( const T& x, const T& y, const T& z )
{
    Vec<T, 3> v;
    v[0] = x;
    v[1] = y;
    v[2] = z;
    return v;
}

template <typename T>
Vec<T, 2> Vec2Init( const T& x, const T& y )
{
    Vec<T, 2> v;
    v[0] = x;
    v[1] = y;
    return v;
}

template<typename R, typename A, size_t N>
void VecInit( const Vec<A,N>& a, Vec<R,N>& m )
{
    for ( size_t i = 0; i < N; i++ )
        m[i] = R( a[i] );
}

template <typename T, size_t N>
class Vec
{
public:
    Vec()
    {
        SetNull( m_v );
    }

    Vec( const T* a )
    {
        Init( m_v, a );
    }

    
    Vec( const T& x, const T& y = 0, const T& z = 0, const T& w = 0 )
    {
        init( x, y, z, w );
    }

    static void Init( T* a, const T* b )
    {
        for ( size_t i = 0; i < N; i++ )
            a[i] = b[i];
    }

    static void SetNull( T* a )
    {
        for ( size_t i = 0; i < N; i++ )
            a[i] = 0;
    }

    static bool IsNull( T* a )
    {
        for ( size_t i = 0; i < N; i++ )
        {
            if ( a[i] != T( 0 ) )
                return false;
        }

        return true;
    }

    static void Invert( T* a )
    {
        for ( size_t i = 0; i < N; i++ )
            a[i] = -a[i];
    }

    static void Mul( T* a, T k )
    {
        for ( size_t i = 0; i < N; i++ )
            a[i] *= k;
    }

    static void Divide( T* a, T k )
    {
        if ( k == 0 )
            return;

        for ( size_t i = 0; i < N; i++ )
            a[i] /= k;
    }

    static T Normalize( T* a )
    {
        T l = Len( a );

        if ( l > 0 )
        {
            for ( size_t i = 0; i < N; i++ )
                a[i] /= l;
        }
        else
        {
            l = 0;
            for ( size_t i = 0; i < N; i++ )
                a[i] = 0;
        }

        return l;
    }

    static void Add( T* a, const T* b )
    {
        for ( size_t i = 0; i < N; i++ )
            a[i] += b[i];
    }

    static void Sub( T* a, const T* b )
    {
        for ( size_t i = 0; i < N; i++ )
            a[i] -= b[i];
    }

    static T DotProduct( const T* a, const T* b )
    {
        T sp = 0;
        for ( size_t i = 0; i < N; i++ )
            sp += a[i]*b[i];

        return sp;
    }

    static void CrossProduct( const T* xyz0, const T* xyz1, T* xyz_res )
    {
        const bool three_dim = N == 3;
        if ( three_dim )
        {
            xyz_res[0] = xyz0[1] * xyz1[2] - xyz0[2] * xyz1[1];
            xyz_res[1] = xyz0[2] * xyz1[0] - xyz0[0] * xyz1[2];
            xyz_res[2] = xyz0[0] * xyz1[1] - xyz0[1] * xyz1[0];
        }
        else
            SetNull( xyz_res );
    }

    static void Rotate( T* xyz, const T* v_, const T& angle )
    {
        const bool three_dim = N == 3;
        if ( three_dim )
        {
            T v[3] = { v_[0], v_[1], v_[2] };
            Normalize( v );

            T cost = T( std::cos( angle ) );
            T sint = T( std::sin( angle ) );
            
            T m[9];
            m[0] = cost + ( 1 - cost ) * v[0] * v[0];
            m[1] = ( 1 - cost ) * v[1] * v[0] + sint * v[2];
            m[2] = ( 1 - cost ) * v[2] * v[0] - sint * v[1];
            m[3] = ( 1 - cost ) * v[0] * v[1] - sint * v[2];
            m[4] = cost + ( 1 - cost ) * v[1] * v[1];
            m[5] = ( 1 - cost ) * v[2] * v[1] + sint * v[0];
            m[6] = ( 1 - cost ) * v[0] * v[2] + sint * v[1];
            m[7] = ( 1 - cost ) * v[1] * v[2] - sint * v[0];
            m[8] = cost + ( 1 - cost ) * v[2] * v[2];
            
            T x = xyz[0];
            T y = xyz[1];
            T z = xyz[2];
            
            xyz[0] = m[0]*x + m[3]*y + m[6]*z;
            xyz[1] = m[1]*x + m[4]*y + m[7]*z;
            xyz[2] = m[2]*x + m[5]*y + m[8]*z;
        }
    }

    static T DistanceSquared( const T* a, const T* b )
    {
        T d2 = 0;
        for ( size_t i = 0; i < N; i++ )
            d2 += ( a[i] - b[i] ) * ( a[i] - b[i] );

        return d2;
    }

    static T Distance( const T* a, const T* b )
    {
        return std::sqrt( DistanceSquared( a, b ) );
    }

    static T LenSquared( const T* a )
    {
        T l2 = 0;
        for ( size_t i = 0; i < N; i++ )
            l2 += a[i]*a[i];

        return l2;
    }

    static T Len( const T* a )
    {
        return std::sqrt( LenSquared( a ) );
    }

    static bool IsEqual( const T* a, const T* b, const T& tolerance )
    {
        return DistanceSquared( a, b ) < ( tolerance * tolerance );
    }
    
    inline size_t dim() const
    {
        return N;
    }

    void init( const T& x, const T& y = 0, const T& z = 0, const T& w = 0 )
    {
        const bool plus0 = N > 0;
        const bool plus1 = N > 1;
        const bool plus2 = N > 2;
        const bool plus3 = N > 3;

        if ( plus0 )
        {
            m_v[0] = x;
            if ( plus1 )
            {
                m_v[1] = y;
                if ( plus2 )
                {
                    m_v[2] = z;
                    if ( plus3 )
                        m_v[3] = w;
                }
            }
        }
    }

    void set( size_t i, const T& v )
    {
        m_v[i] = v;
    }

    const T& get( size_t i ) const
    {
        return m_v[i];
    }

    T& get( size_t i )
    {
        return m_v[i];
    }

    void setNull()
    {
        SetNull( m_v );
    }

    void isNull() const
    {
        IsNull( m_v );
    }

    void invert()
    {
        Invert( m_v );
    }

    T lenSquared() const
    {
        return LenSquared( m_v );
    }

    T len() const
    {
        return std::sqrt( LenSquared( m_v ) );
    }

    T distanceSquared( const T* a ) const
    {
        return DistanceSquared( m_v, a );
    }

    T distance( const T* a ) const
    {
        return std::sqrt( DistanceSquared( m_v, a ) );
    }

    bool isEqual( const T* b, const T& tolerance ) const
    {
        return DistanceSquared( m_v, b ) < ( tolerance * tolerance );
    }

    T normalize()
    {
        return Normalize( m_v );
    }

    void extract( T* data ) const
    {
        for ( size_t i = 0; i < N; i++ )
            data[i] = m_v[i];
    }

    T dotProduct( const Vec& v ) const
    {
        return DotProduct( m_v, v.m_v );
    }

    Vec crossProduct( const Vec &v ) const
    {
        Vec res;
        CrossProduct( m_v, v.m_v, res.m_v );
        return res;
    }

    void rotate( const T* v_, const T& angle )
    {
        Rotate( m_v, v_, angle );
    }

    inline const T* data() const
    {
        return m_v;
    }

    inline T* data()
    {
        return m_v;
    }

    bool operator == ( const Vec &v ) const
    {
        for ( size_t i = 0; i < N; i++ )
        {
            if ( m_v[i] != v.m_v[i] )
                return false;
        }

        return true;
    }

    bool operator != ( const Vec &v ) const
    {
        for ( size_t i = 0; i < N; i++ )
        {
            if ( m_v[i] != v.m_v[i] )
                return true;
        }
    
        return false;
    }

    Vec& operator = ( const Vec &v )
    {
        for ( size_t i = 0; i < N; i++ )
            m_v[i] = v.m_v[i];

        return *this;
    }

    Vec operator + ( const Vec &v ) const
    {
        Vec res;
        for ( size_t i = 0; i < N; i++ )
            res.m_v[i] = m_v[i] + v.m_v[i];

        return res;
    }

    Vec operator + ( const T* v ) const
    {
        Vec res;
        for ( size_t i = 0; i < N; i++ )
            res.m_v[i] = m_v[i] + v[i];

        return res;
    }

    Vec operator - () const
    {
        Vec res;
        for ( size_t i = 0; i < N; i++ )
            res.m_v[i] = -m_v[i];

        return res;
    }

    Vec operator - ( const Vec &v ) const
    {
        Vec res;
        for ( size_t i = 0; i < N; i++ )
            res.m_v[i] = m_v[i] - v.m_v[i];

        return res;
    }

    Vec operator - ( const T* v ) const
    {
        Vec res;
        for ( size_t i = 0; i < N; i++ )
            res.m_v[i] = m_v[i] - v[i];

        return res;
    }

    Vec operator * ( const T& k ) const
    {
        Vec res;
        for ( size_t i = 0; i < N; i++ )
            res.m_v[i] = m_v[i]*k;

        return res;
    }

    T operator * ( const Vec& k ) const
    {
        T res = 0;
        for ( size_t i = 0; i < N; i++ )
            res += m_v[i] * k.m_v[i];
        return res;
    }

    T operator * ( const T* v ) const
    {
        T res = 0;
        for ( size_t i = 0; i < N; i++ )
            res += m_v[i] * v[i];
        return res;
    }

    Vec operator / ( T k ) const
    {
        Vec res;
        if ( VERIFY( std::abs( k ) > 0 ) )
        {
            for ( size_t i = 0; i < N; i++ )
                res.m_v[i] = m_v[i] / k;
        }

        return res;
    }

    inline const T& operator[]( size_t i ) const
    {
        return m_v[i];
    }

    inline const T& operator[]( int i ) const
    {
        return m_v[i];
    }

    inline T& operator[]( size_t i )
    {
        return m_v[i];
    }

    inline T& operator[]( int i )
    {
        return m_v[i];
    }

    Vec& operator += ( const Vec& v )
    {
        for ( size_t i = 0; i < N; i++ )
            m_v[i] += v.m_v[i];

        return *this;
    }

    Vec& operator -= ( const Vec& v )
    {
        for ( size_t i = 0; i < N; i++ )
            m_v[i] -= v.m_v[i];

        return *this;
    }

    Vec& operator *= ( T k )
    {
        for ( size_t i = 0; i < N; i++ )
            m_v[i] *= k;

        return *this;
    }

    Vec& operator /= ( T k )
    {
        for ( size_t i = 0; i < N; i++ )
            m_v[i] /= k;

        return *this;
    }

    inline operator const T* () const
    {
        return m_v;
    }

    inline operator T* ()
    {
        return m_v;
    }

private:
    T m_v[N];
};

template<size_t N> using VecD = Vec<double,N>;
template<size_t N> using VecF = Vec<float,N>;

template<typename T> using Vec2 = Vec<T,2>;
template<typename T> using Vec3 = Vec<T,3>;
template<typename T> using Vec4 = Vec<T,4>;

using VecD4 = Vec4<double>;
using VecD3 = Vec3<double>;
using VecD2 = Vec2<double>;
using VecD4Opt = VarOpt<VecD4>;
using VecD3Opt = VarOpt<VecD3>;
using VecD2Opt = VarOpt<VecD2>;

using VecF4 = Vec4<float>;
using VecF3 = Vec3<float>;
using VecF2 = Vec2<float>;
using VecF4Opt = VarOpt<VecF4>;
using VecF3Opt = VarOpt<VecF3>;
using VecF2Opt = VarOpt<VecF2>;

using VecI4 = Vec4<int>;
using VecI3 = Vec3<int>;
using VecI2 = Vec2<int>;
using VecI4Opt = VarOpt<VecI4>;
using VecI3Opt = VarOpt<VecI3>;
using VecI2Opt = VarOpt<VecI2>;

using VecUC4 = Vec4<unsigned char>;
using VecUC3 = Vec3<unsigned char>;

}

