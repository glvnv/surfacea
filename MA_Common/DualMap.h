#pragma once

#include <map>

namespace MA
{

template <typename A, typename B>
class DualMap
{
public:
    A* firstBySecond( const B& b )
    {
        auto i = m_b_to_a.find( b );
        return ( i != m_b_to_a.end() ) ? &( i->second ) : nullptr;
    }

    B* secondByFirst( const A& a )
    {
        auto i = m_a_to_b.find( a );
        return ( i != m_a_to_b.end() ) ? &( i->second ) : nullptr;
    }

    const A* firstBySecond( const B& b ) const
    {
        auto i = m_b_to_a.find( b );
        return ( i != m_b_to_a.end() ) ? &( i->second ) : nullptr;
    }

    const B* secondByFirst( const A& a ) const
    {
        auto i = m_a_to_b.find( a );
        return ( i != m_a_to_b.end() ) ? &( i->second ) : nullptr;
    }

    void add( const A& a, const B& b )
    {
        m_a_to_b.emplace( std::make_pair( a, b ) );
        m_b_to_a.emplace( std::make_pair( b, a ) );
    }

    void erase( const A& a )
    {
        m_b_to_a.erase( m_a_to_b.at( a ) );
        m_a_to_b.erase( a );
    }

    void erase( const B& b )
    {
        m_a_to_b.erase( m_b_to_a.at( b ) );
        m_b_to_a.erase( b );
    }

    void clear()
    {
        m_a_to_b.clear();
        m_b_to_a.clear();
    }

    const std::map<A, B>& forwardMap() const
    {
        return m_a_to_b;
    }

    size_t size() const
    {
        return m_a_to_b.size();
    }

private:
    std::map<A, B> m_a_to_b;
    std::map<B, A> m_b_to_a;
};

}
