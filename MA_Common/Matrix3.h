#pragma once

namespace MA
{

template <typename T>
class Matrix3
{
public:
    Matrix3()
    {
        setIdentity();
    }

    Matrix3( const T* data )
    {
        set( data );
    }

    void set( const T* data )
    {
        is_identity = false;
        for ( size_t i = 0; i < 9; i++ )
            a[i] = data[i];
    }

    void transpose()
    {
        std::swap( at( 0, 1 ), at( 1, 0 ) );
        std::swap( at( 0, 2 ), at( 2, 0 ) );
        std::swap( at( 1, 2 ), at( 2, 1 ) );
    }

    void setIdentity()
    {
        is_identity = true;
        at( 0, 0 ) = 1;
        at( 1, 0 ) = 0;
        at( 2, 0 ) = 0;
        at( 0, 1 ) = 0;
        at( 1, 1 ) = 1;
        at( 2, 1 ) = 0;
        at( 0, 2 ) = 0;
        at( 1, 2 ) = 0;
        at( 2, 2 ) = 1;
    }

    bool isIdentity() const
    {
        if ( is_identity )
            return true;

        const bool is_i = ( at( 0, 0 ) == 1 &&
                            at( 1, 0 ) == 0 &&
                            at( 2, 0 ) == 0 &&
                            at( 0, 1 ) == 0 &&
                            at( 1, 1 ) == 1 &&
                            at( 2, 1 ) == 0 &&
                            at( 0, 2 ) == 0 &&
                            at( 1, 2 ) == 0 &&
                            at( 2, 2 ) == 1 )

        is_identity = is_i;
        return is_identity;
    }

    T& at( size_t i, size_t j )
    {
        is_identity = false;
        return a[i + 3*j];
    }

    const T& at( size_t i, size_t j ) const
    {
        return a[i + 3*j];
    }

    T& operator [] ( size_t i )
    {
        is_identity = false;
        return a[i];
    }

    const T& operator [] ( size_t i ) const
    {
        return a[i];
    }

    T* data()
    {
        is_identity = false;
        return a;
    }

    const T* data() const
    {
        return a;
    }

private:
    mutable bool is_identity;
    T a[9];
};

using MatrixD3 = Matrix3<double>;
using MatrixF3 = Matrix3<float>;

}
