#pragma once

#include "Vec.h"
#include <array>

namespace MA
{

template<typename T> class Matrix4;

template<typename R, typename A>
void Init( const Matrix4<A>& a, Matrix4<R>& m )
{
    for ( size_t i = 0; i < 16; i++ )
        m[i] = R( a[i] );
}

template<typename R, typename A, typename B>
void Multiply( const Matrix4<A>& a, const Matrix4<B>& b, Matrix4<R>& m )
{
    m.at( 0, 0 ) = R( a.at( 0, 0 ) * b.at( 0, 0 ) + a.at( 0, 1 ) * b.at( 1, 0 ) + a.at( 0, 2 ) * b.at( 2, 0 ) + a.at( 0, 3 ) * b.at( 3, 0 ) );
    m.at( 1, 0 ) = R( a.at( 1, 0 ) * b.at( 0, 0 ) + a.at( 1, 1 ) * b.at( 1, 0 ) + a.at( 1, 2 ) * b.at( 2, 0 ) + a.at( 1, 3 ) * b.at( 3, 0 ) );
    m.at( 2, 0 ) = R( a.at( 2, 0 ) * b.at( 0, 0 ) + a.at( 2, 1 ) * b.at( 1, 0 ) + a.at( 2, 2 ) * b.at( 2, 0 ) + a.at( 2, 3 ) * b.at( 3, 0 ) );
    m.at( 3, 0 ) = R( a.at( 3, 0 ) * b.at( 0, 0 ) + a.at( 3, 1 ) * b.at( 1, 0 ) + a.at( 3, 2 ) * b.at( 2, 0 ) + a.at( 3, 3 ) * b.at( 3, 0 ) );

    m.at( 0, 1 ) = R( a.at( 0, 0 ) * b.at( 0, 1 ) + a.at( 0, 1 ) * b.at( 1, 1 ) + a.at( 0, 2 ) * b.at( 2, 1 ) + a.at( 0, 3 ) * b.at( 3, 1 ) );
    m.at( 1, 1 ) = R( a.at( 1, 0 ) * b.at( 0, 1 ) + a.at( 1, 1 ) * b.at( 1, 1 ) + a.at( 1, 2 ) * b.at( 2, 1 ) + a.at( 1, 3 ) * b.at( 3, 1 ) );
    m.at( 2, 1 ) = R( a.at( 2, 0 ) * b.at( 0, 1 ) + a.at( 2, 1 ) * b.at( 1, 1 ) + a.at( 2, 2 ) * b.at( 2, 1 ) + a.at( 2, 3 ) * b.at( 3, 1 ) );
    m.at( 3, 1 ) = R( a.at( 3, 0 ) * b.at( 0, 1 ) + a.at( 3, 1 ) * b.at( 1, 1 ) + a.at( 3, 2 ) * b.at( 2, 1 ) + a.at( 3, 3 ) * b.at( 3, 1 ) );

    m.at( 0, 2 ) = R( a.at( 0, 0 ) * b.at( 0, 2 ) + a.at( 0, 1 ) * b.at( 1, 2 ) + a.at( 0, 2 ) * b.at( 2, 2 ) + a.at( 0, 3 ) * b.at( 3, 2 ) );
    m.at( 1, 2 ) = R( a.at( 1, 0 ) * b.at( 0, 2 ) + a.at( 1, 1 ) * b.at( 1, 2 ) + a.at( 1, 2 ) * b.at( 2, 2 ) + a.at( 1, 3 ) * b.at( 3, 2 ) );
    m.at( 2, 2 ) = R( a.at( 2, 0 ) * b.at( 0, 2 ) + a.at( 2, 1 ) * b.at( 1, 2 ) + a.at( 2, 2 ) * b.at( 2, 2 ) + a.at( 2, 3 ) * b.at( 3, 2 ) );
    m.at( 3, 2 ) = R( a.at( 3, 0 ) * b.at( 0, 2 ) + a.at( 3, 1 ) * b.at( 1, 2 ) + a.at( 3, 2 ) * b.at( 2, 2 ) + a.at( 3, 3 ) * b.at( 3, 2 ) );

    m.at( 0, 3 ) = R( a.at( 0, 0 ) * b.at( 0, 3 ) + a.at( 0, 1 ) * b.at( 1, 3 ) + a.at( 0, 2 ) * b.at( 2, 3 ) + a.at( 0, 3 ) * b.at( 3, 3 ) );
    m.at( 1, 3 ) = R( a.at( 1, 0 ) * b.at( 0, 3 ) + a.at( 1, 1 ) * b.at( 1, 3 ) + a.at( 1, 2 ) * b.at( 2, 3 ) + a.at( 1, 3 ) * b.at( 3, 3 ) );
    m.at( 2, 3 ) = R( a.at( 2, 0 ) * b.at( 0, 3 ) + a.at( 2, 1 ) * b.at( 1, 3 ) + a.at( 2, 2 ) * b.at( 2, 3 ) + a.at( 2, 3 ) * b.at( 3, 3 ) );
    m.at( 3, 3 ) = R( a.at( 3, 0 ) * b.at( 0, 3 ) + a.at( 3, 1 ) * b.at( 1, 3 ) + a.at( 3, 2 ) * b.at( 2, 3 ) + a.at( 3, 3 ) * b.at( 3, 3 ) );
}

template <typename T>
inline T Det3( T e00, T e10, T e20, T e01, T e11, T e21, T e02, T e12, T e22 )
{
    return ( e00 * e11 * e22 ) + ( e01 * e12 * e20 ) + ( e10 * e21 * e02 ) - ( e02 * e11 * e20 ) - ( e01 * e10 * e22 ) - ( e00 * e21 * e12 );
}

template<typename R, typename A>
void Inverse( const Matrix4<A>& a, Matrix4<R>& m )
{
    double det = double( a.det4() );
    if ( det == 0 )
    {
        m = Matrix4<R>();
        return;
    }

    m.at( 0, 0 ) = + R( Det3( a.at( 1, 1 ), a.at( 2, 1 ), a.at( 3, 1 ), a.at( 1, 2 ), a.at( 2, 2 ), a.at( 3, 2 ), a.at( 1, 3 ), a.at( 2, 3 ), a.at( 3, 3 ) ) );
    m.at( 1, 0 ) = - R( Det3( a.at( 1, 0 ), a.at( 2, 0 ), a.at( 3, 0 ), a.at( 1, 2 ), a.at( 2, 2 ), a.at( 3, 2 ), a.at( 1, 3 ), a.at( 2, 3 ), a.at( 3, 3 ) ) );
    m.at( 2, 0 ) = + R( Det3( a.at( 1, 0 ), a.at( 2, 0 ), a.at( 3, 0 ), a.at( 1, 1 ), a.at( 2, 1 ), a.at( 3, 1 ), a.at( 1, 3 ), a.at( 2, 3 ), a.at( 3, 3 ) ) );
    m.at( 3, 0 ) = - R( Det3( a.at( 1, 0 ), a.at( 2, 0 ), a.at( 3, 0 ), a.at( 1, 1 ), a.at( 2, 1 ), a.at( 3, 1 ), a.at( 1, 2 ), a.at( 2, 2 ), a.at( 3, 2 ) ) );

    m.at( 0, 1 ) = - R( Det3( a.at( 0, 1 ), a.at( 2, 1 ), a.at( 3, 1 ), a.at( 0, 2 ), a.at( 2, 2 ), a.at( 3, 2 ), a.at( 0, 3 ), a.at( 2, 3 ), a.at( 3, 3 ) ) );
    m.at( 1, 1 ) = + R( Det3( a.at( 0, 0 ), a.at( 2, 0 ), a.at( 3, 0 ), a.at( 0, 2 ), a.at( 2, 2 ), a.at( 3, 2 ), a.at( 0, 3 ), a.at( 2, 3 ), a.at( 3, 3 ) ) );
    m.at( 2, 1 ) = - R( Det3( a.at( 0, 0 ), a.at( 2, 0 ), a.at( 3, 0 ), a.at( 0, 1 ), a.at( 2, 1 ), a.at( 3, 1 ), a.at( 0, 3 ), a.at( 2, 3 ), a.at( 3, 3 ) ) );
    m.at( 3, 1 ) = + R( Det3( a.at( 0, 0 ), a.at( 2, 0 ), a.at( 3, 0 ), a.at( 0, 1 ), a.at( 2, 1 ), a.at( 3, 1 ), a.at( 0, 2 ), a.at( 2, 2 ), a.at( 3, 2 ) ) );

    m.at( 0, 2 ) = + R( Det3( a.at( 0, 1 ), a.at( 1, 1 ), a.at( 3, 1 ), a.at( 0, 2 ), a.at( 1, 2 ), a.at( 3, 2 ), a.at( 0, 3 ), a.at( 1, 3 ), a.at( 3, 3 ) ) );
    m.at( 1, 2 ) = - R( Det3( a.at( 0, 0 ), a.at( 1, 0 ), a.at( 3, 0 ), a.at( 0, 2 ), a.at( 1, 2 ), a.at( 3, 2 ), a.at( 0, 3 ), a.at( 1, 3 ), a.at( 3, 3 ) ) );
    m.at( 2, 2 ) = + R( Det3( a.at( 0, 0 ), a.at( 1, 0 ), a.at( 3, 0 ), a.at( 0, 1 ), a.at( 1, 1 ), a.at( 3, 1 ), a.at( 0, 3 ), a.at( 1, 3 ), a.at( 3, 3 ) ) );
    m.at( 3, 2 ) = - R( Det3( a.at( 0, 0 ), a.at( 1, 0 ), a.at( 3, 0 ), a.at( 0, 1 ), a.at( 1, 1 ), a.at( 3, 1 ), a.at( 0, 2 ), a.at( 1, 2 ), a.at( 3, 2 ) ) );

    m.at( 0, 3 ) = - R( Det3( a.at( 0, 1 ), a.at( 1, 1 ), a.at( 2, 1 ), a.at( 0, 2 ), a.at( 1, 2 ), a.at( 2, 2 ), a.at( 0, 3 ), a.at( 1, 3 ), a.at( 2, 3 ) ) );
    m.at( 1, 3 ) = + R( Det3( a.at( 0, 0 ), a.at( 1, 0 ), a.at( 2, 0 ), a.at( 0, 2 ), a.at( 1, 2 ), a.at( 2, 2 ), a.at( 0, 3 ), a.at( 1, 3 ), a.at( 2, 3 ) ) );
    m.at( 2, 3 ) = - R( Det3( a.at( 0, 0 ), a.at( 1, 0 ), a.at( 2, 0 ), a.at( 0, 1 ), a.at( 1, 1 ), a.at( 2, 1 ), a.at( 0, 3 ), a.at( 1, 3 ), a.at( 2, 3 ) ) );
    m.at( 3, 3 ) = + R( Det3( a.at( 0, 0 ), a.at( 1, 0 ), a.at( 2, 0 ), a.at( 0, 1 ), a.at( 1, 1 ), a.at( 2, 1 ), a.at( 0, 2 ), a.at( 1, 2 ), a.at( 2, 2 ) ) );

    for ( size_t i = 0; i < 16; i++ )
        m[i] = R( double( m[i] ) / det );
}

template <typename T>
class Matrix4
{
public:
    Matrix4()
    {
        setIdentity();
    }

    Matrix4( const T* data )
    {
        set( data );
    }

    void set( const T* data )
    {
        is_identity = false;
        for ( size_t i = 0; i < 16; i++ )
            a[i] = data[i];
    }

    void perspective( T left, T right, T bottom, T top, T near_clip, T far_clip )
    {        
        at( 0, 0 ) = ( static_cast<T>(2) * near_clip ) / ( right - left );
        at( 1, 0 ) = 0;
        at( 2, 0 ) = 0;
        at( 3, 0 ) = 0;
        at( 0, 1 ) = 0;
        at( 1, 1 ) = ( static_cast<T>(2) * near_clip ) / ( top - bottom );
        at( 2, 1 ) = 0;
        at( 3, 1 ) = 0;
        at( 0, 2 ) = ( right + left ) / ( right - left );
        at( 1, 2 ) = ( top + bottom ) / ( top - bottom );
        at( 2, 2 ) = ( far_clip + near_clip ) / ( near_clip - far_clip );
        at( 3, 2 ) = static_cast<T>(-1);
        at( 0, 3 ) = 0;
        at( 1, 3 ) = 0;
        at( 2, 3 ) = ( static_cast<T>(2) * far_clip * near_clip ) / ( near_clip - far_clip );
        at( 3, 3 ) = 0;
    }

    void ortho( T width, T height, T near_clip, T far_clip )
    {        
        at( 0, 0 ) = static_cast<T>(1) / width;
        at( 0, 1 ) = 0;
        at( 0, 2 ) = 0;
        at( 0, 3 ) = 0;
        at( 1, 0 ) = 0;
        at( 1, 1 ) = static_cast<T>(1) / height;
        at( 1, 2 ) = 0;
        at( 1, 3 ) = 0;
        at( 2, 0 ) = 0;
        at( 2, 1 ) = 0;
        at( 2, 2 ) = static_cast<T>(2) / ( near_clip - far_clip );
        at( 2, 3 ) = ( near_clip + far_clip ) / ( near_clip - far_clip );
        at( 3, 0 ) = 0;
        at( 3, 1 ) = 0;
        at( 3, 2 ) = 0;
        at( 3, 3 ) = 1;
    }

    void transpose()
    {
        std::swap( at( 0, 1 ), at( 1, 0 ) );
        std::swap( at( 0, 2 ), at( 2, 0 ) );
        std::swap( at( 0, 3 ), at( 3, 0 ) );

        std::swap( at( 1, 2 ), at( 2, 1 ) );
        std::swap( at( 1, 3 ), at( 3, 1 ) );        

        std::swap( at( 2, 3 ), at( 3, 2 ) );
    }

    Matrix4 inverse() const
    {
        Matrix4 m;
        Inverse<T>( *this, m );
        return m;
    }

    void lookAt( const Vec3<T>& eye, const Vec3<T>& aim, const Vec3<T>& up )
    {
        Vec3<T> f = aim - eye;
        f.normalize();

        Vec3<T> s = f.crossProduct( up );
        s.normalize();

        Vec3<T> u = s.crossProduct( f );

        at( 0, 0 ) =  s[0];
        at( 0, 1 ) =  s[1];
        at( 0, 2 ) =  s[2];
        at( 1, 0 ) =  u[0];
        at( 3, 0 ) =  0;
        at( 1, 1 ) =  u[1];
        at( 1, 2 ) =  u[2];
        at( 2, 0 ) = -f[0];
        at( 2, 1 ) = -f[1];
        at( 3, 1 ) =  0;
        at( 2, 2 ) = -f[2];
        at( 3, 2 ) =  0;
        at( 0, 3 ) = -( s * eye );
        at( 1, 3 ) = -( u * eye );
        at( 2, 3 ) = f * eye;
        at( 3, 3 ) = 1;
    }

    void setIdentity()
    {
        is_identity = true;
        at( 0, 0 ) = 1;
        at( 1, 0 ) = 0;
        at( 2, 0 ) = 0;
        at( 3, 0 ) = 0;
        at( 0, 1 ) = 0;
        at( 1, 1 ) = 1;
        at( 2, 1 ) = 0;
        at( 3, 1 ) = 0;
        at( 0, 2 ) = 0;
        at( 1, 2 ) = 0;
        at( 2, 2 ) = 1;
        at( 3, 2 ) = 0;
        at( 0, 3 ) = 0;
        at( 1, 3 ) = 0;
        at( 2, 3 ) = 0;
        at( 3, 3 ) = 1;
    }

    bool isIdentity() const
    {
        if ( is_identity )
            return true;

        const bool is_i = ( at( 0, 0 ) == 1 &&
                            at( 1, 0 ) == 0 &&
                            at( 2, 0 ) == 0 &&
                            at( 3, 0 ) == 0 &&
                            at( 0, 1 ) == 0 &&
                            at( 1, 1 ) == 1 &&
                            at( 2, 1 ) == 0 &&
                            at( 3, 1 ) == 0 &&
                            at( 0, 2 ) == 0 &&
                            at( 1, 2 ) == 0 &&
                            at( 2, 2 ) == 1 &&
                            at( 3, 2 ) == 0 &&
                            at( 0, 3 ) == 0 &&
                            at( 1, 3 ) == 0 &&
                            at( 2, 3 ) == 0 &&
                            at( 3, 3 ) == 1 );

        is_identity = is_i;
        return is_identity;
    }

    Vec3<T> rotate( const Vec3<T>& v ) const
    {
        Vec3<T> res;

        res[0] = ( a[0] * v[0] ) + ( a[4] * v[1] ) + ( a[8 ] * v[2] );
        res[1] = ( a[1] * v[0] ) + ( a[5] * v[1] ) + ( a[9 ] * v[2] );
        res[2] = ( a[2] * v[0] ) + ( a[6] * v[1] ) + ( a[10] * v[2] );

        return res;
    }

    Matrix4 operator * ( const Matrix4& b ) const
    {
        Matrix4 m;
        Multiply( *this, b, m );
        return m;
    }

    Vec4<T> operator * ( const Vec4<T>& v ) const
    {
        Vec4<T> res;

        res[0] = ( a[0] * v[0] ) + ( a[4] * v[1] ) + ( a[8 ] * v[2] ) + ( a[12] * v[3] );
        res[1] = ( a[1] * v[0] ) + ( a[5] * v[1] ) + ( a[9 ] * v[2] ) + ( a[13] * v[3] );
        res[2] = ( a[2] * v[0] ) + ( a[6] * v[1] ) + ( a[10] * v[2] ) + ( a[14] * v[3] );
        res[3] = ( a[3] * v[0] ) + ( a[7] * v[1] ) + ( a[11] * v[2] ) + ( a[15] * v[3] );

        return res;
    }

    Vec3<T> operator * ( const Vec3<T>& v ) const
    {
        Vec3<T> res;

        res[0] = ( a[0] * v[0] ) + ( a[4] * v[1] ) + ( a[8 ] * v[2] ) + a[12];
        res[1] = ( a[1] * v[0] ) + ( a[5] * v[1] ) + ( a[9 ] * v[2] ) + a[13];
        res[2] = ( a[2] * v[0] ) + ( a[6] * v[1] ) + ( a[10] * v[2] ) + a[14];

        return res;
    }

    bool operator == ( const Matrix4& v ) const
    {
        return a == v.a;
    }

    T det4() const
    {
        return  ( at( 0, 0 ) * Det3( at( 1, 1 ), at( 2, 1 ), at( 3, 1 ), at( 1, 2 ), at( 2, 2 ), at( 3, 2 ), at( 1, 3 ), at( 2, 3 ), at( 3, 3 ) )
                - at( 1, 0 ) * Det3( at( 0, 1 ), at( 2, 1 ), at( 3, 1 ), at( 0, 2 ), at( 2, 2 ), at( 3, 2 ), at( 0, 3 ), at( 2, 3 ), at( 3, 3 ) )
                + at( 2, 0 ) * Det3( at( 0, 1 ), at( 1, 1 ), at( 3, 1 ), at( 0, 2 ), at( 1, 2 ), at( 3, 2 ), at( 0, 3 ), at( 1, 3 ), at( 3, 3 ) )
                - at( 3, 0 ) * Det3( at( 0, 1 ), at( 1, 1 ), at( 2, 1 ), at( 0, 2 ), at( 1, 2 ), at( 2, 2 ), at( 0, 3 ), at( 1, 3 ), at( 2, 3 ) ) );
    }

    T& at( size_t i, size_t j )
    {
        is_identity = false;
        return a[i + 4*j];
    }

    const T& at( size_t i, size_t j ) const
    {
        return a[i + 4*j];
    }

    T& operator [] ( size_t i )
    {
        is_identity = false;
        return a[i];
    }

    const T& operator [] ( size_t i ) const
    {
        return a[i];
    }

    T* data()
    {
        is_identity = false;
        return a.data();
    }

    const T* data() const
    {
        return a.data();
    }

private:
    mutable bool is_identity;
    std::array<T, 16> a;
};

using MatrixD4 = Matrix4<double>;
using MatrixF4 = Matrix4<float>;

}
