#pragma once

#include "Vec.h"
#include "MathUtils.h"

namespace MA
{

template <typename V>
void CalcPolyParams( const std::vector<V>& points, std::vector<double>& params )
{
    params.clear();
    if ( points.empty() )
        return;

    params.resize( points.size() );
    params.front() = 0;
    for ( size_t i = 1; i < points.size(); i++ )
        params[i] = params[i-1] + std::sqrt( double( V::Distance( points[i], points[i-1] ) ) );
}

namespace Line
{
template <typename V, typename T>
V PointOn( const V& point, const V& dir, const T& t )
{
    return ( dir * t ) + point;
}

template <typename T, typename V>
void FindNearestPointOn( const T& origin, const T& dir, const T& p, T* p_on, V* curve_t )
{
    ASSERT ( p_on || curve_t );

    const V t = dir * ( p - origin );
    if ( curve_t )
        *curve_t = t;

    if ( p_on )
        *p_on = PointOn( origin, dir, t );
}
}

namespace Circle
{

template <typename V, typename T>
V PointOn( const V& point, const V& x_dir, const V& y_dir, const T& radius, const T& t )
{
    return point + ( ( ( x_dir * std::cos( t ) ) + ( y_dir * std::sin( t ) ) ) * radius );
}

}

namespace Plane
{
    template <typename T>
    Vec3<T> PointOn( const Vec3<T>& origin, const Vec3<T>& x_dir, const Vec3<T>& y_dir, const T& u, const T& v )
    {
        return origin + ( x_dir * u ) + ( y_dir * v );
    }

    template <typename T>
    void ProjOn( const Vec3<T>& origin, const Vec3<T>& x_dir, const Vec3<T>& y_dir, const Vec3<T>& point, T& u, T& v )
    {
        const Vec3<T> a = point - origin;
        u = Vec3<T>::DotProduct( x_dir, a );
        v = Vec3<T>::DotProduct( y_dir, a );
    }

    template <typename T>
    T DistanceTo( const Vec3<T>& origin, const Vec3<T>& x_dir, const Vec3<T>& y_dir, const Vec3<T>& point )
    {
        VecD3 n = x_dir.crossProduct( y_dir );
        if ( n.normalize() < T( DBL_EPSILON ) )
            return 0;

        const Vec3<T> a = point - origin;
        return a.dotProduct( n );
    }
}

namespace Sphere
{
template <typename T>
Vec3<T> PointOn( const Vec3<T>& x_dir, const Vec3<T>& y_dir, const Vec3<T>& z_dir, const T& u, const T& v )
{
    return ( ( ( x_dir * std::cos( u ) ) + ( y_dir * std::sin( u ) ) ) * std::cos( v ) ) + ( z_dir * std::sin( v ) );
}

template <typename T>
void ProjOn( const Vec3<T>& x_dir, const Vec3<T>& y_dir, const Vec3<T>& z_dir, const Vec3<T>& point, T& u, T& v )
{
    u = get_angle( point, x_dir, y_dir );
    
    Vec3<T> w;
    rotate( x_dir, y_dir, u, w );
    v = get_angle( point, w, z_dir );
    if ( v > M_PI_2 )
        v -= 2*M_PI;

    ASSERT( std::abs( v ) <= M_PI_2 );
}

}

template<typename T>
bool IsParamInInterval( bool is_periodic, const T& t_geom_begin, const T& t_geom_end, const T& t_begin, const T& t_end, const T& t_, T* t_in = nullptr )
{
    if ( t_end < t_begin )
        throw Exception( Exception::S_IncorrectArgument, "IsParamInInterval t_end < t_begin" );

    T t = t_;
    if ( t >= t_begin && t <= t_end )
    {
        if ( t_in )
            *t_in = t;

        return true;
    }

    if ( is_periodic )
    {
        const double period = t_geom_end - t_geom_begin;
        t -= std::floor( ( t - t_begin ) / period ) * period;
        if ( t >= t_begin && t <= t_end )
        {
            if ( t_in )
                *t_in = t;

            return true;
        }
    }
    return false;
}

template<typename T, size_t N>
T PointToLineSquared( const T p[N], const T line_p[N], const T line_dir[N], T* line_t = nullptr )
{
    Vec<T,N> to = p;
    to -= line_p;
    
    const T line_dir_len = Vec<T,N>::Len( line_dir );
    const T s = Vec<T,N>::DotProduct( to, line_dir ) / line_dir_len;
    if ( line_t )
        *line_t = s / line_dir_len;

    return to.lenSquared() - ( s*s );
}

template<typename T, size_t N>
T PointToLine( const T p[N], const T line_p[N], const T line_dir[N], T* line_t = nullptr )
{
    return std::sqrt( PointToLineSquared<T,N>( p, line_p, line_dir, line_t ) );
}

template<typename T, size_t N>
T PointToSegmentSquared( const T p[N], const T sa[N], const T sb[N], T* t = nullptr )
{
    Vec<T,N> av = p;
    av -= sa;
    
    Vec<T,N> s = sb;
    s -= sa;
    const T s_len = s.normalize();
    if ( s_len <= 0 )
        throw Exception( Exception::S_IncorrectArgument, "PointToSegmentSquared segm null length" );
    
    const T dotpr = av.dotProduct( s );
    
    if ( dotpr > s_len )
    {
        if ( t )
            *t = 1;
        return Vec<T,N>::DistanceSquared( p, sb );
    }
    else if ( dotpr < 0 )
    {
        if ( t )
            *t = 0;
        return av.lenSquared();
    }
    else
    {
        if ( t )
            *t = dotpr / s_len;
        return av.lenSquared() - ( dotpr * dotpr );
    }
}

template<typename T, size_t N>
T PointToSegment( const T p[N], const T sa[N], const T sb[N], T* t = nullptr )
{
    return std::sqrt( PointToSegmentSquared<T,N>( p, sa, sb, t ) );
}

template <typename V, typename T>
V PointOnSegment( const V& p0, const V& p1, const T& t )
{
    return ( p0 * ( 1 - t ) ) + ( p1 * t );
}

template<typename T, size_t N>
T SegmentToLine( const T s0[N], const T s1[N], const T p[N], const T d[N], T& ts, T& tl )
{
    Vec<T,N> p1 = p;
    Vec<T,N> d1 = d;

    Vec<T,N> p2 = s0;
    Vec<T,N> d2 = s1;
    d2 -= s0;
    
    const T a11 = d1 * d1;
    const T a22 = d2 * d2;
    const T a12 = d1 * d2;
    const T det = a11 * a22 - squared( a12 );
    
    if ( det != 0 )
    {
        const T one_det = 1 / det;
        tl = ( ( ( p2 - p1 ) * ( d1 * a22 ) ) - ( ( p2 - p1 ) * ( d2 * a12 ) ) ) * one_det;
        ts = ( ( ( p1 - p2 ) * ( d2 * a11 ) ) - ( ( p1 - p2 ) * ( d1 * a12 ) ) ) * one_det;
    
        if ( ts < 0 )
        {
            ts = 0;
            return PointToLine<T,N>( s0, p, d, &tl );
        }
        else if ( ts > 1 )
        {
            ts = 1;
            return PointToLine<T,N>( s1, p, d, &tl );
        }
        else
        {
            Vec<T,N> pl = d;
            pl *= tl;
            pl += p;
    
            Vec<T,N> ps = s1;
            ps -= s0;
            ps *= ts;
            ps += s0;
    
            return Vec<T,N>::Distance( pl, ps );
        }
    }
    else
    {
        ts = T( 0.5 );
        Vec<T,N> m = s0;
        m += s1;
        return PointToLine<T,N>( m / 2, p, d, &tl );
    }
}

template<typename T, size_t N>
T SegmentToSegment( const T a0[N], const T a1[N], const T b0[N], const T b1[N], T& w0, T& w1 )
{
    Vec<T,N> ua0 = a0;
    Vec<T,N> ua1 = a1;
    Vec<T,N> ub0 = b0;
    Vec<T,N> ub1 = b1;
    
    for ( size_t i = 0; i < N; i++ )
    {
        const double max_val = max_value( a0[i], a1[i], b0[i], b1[i] );
        ua0[i] /= max_val;
        ua1[i] /= max_val;
        ub0[i] /= max_val;
        ub1[i] /= max_val;
    }

    T d0[N];
    Vec<T,N>::Init( d0, ua1 );
    Vec<T,N>::Sub( d0, ua0 );
    
    const T a00 = Vec<T,N>::DotProduct( d0, d0 );
    
    if ( a00 == 0 )
    {
        // segment a is degenerate
        w0 = 0;
        return PointToSegment<T,N>( ua0, ub0, ub1, &w1 );
    }
    
    T d1[N];
    Vec<T,N>::Init( d1, ub1 );
    Vec<T,N>::Sub( d1, ub0 );
    
    const T a11 = Vec<T,N>::DotProduct( d1, d1 );
    if ( a11 == 0 )
    {
        // segment b is degenerate
        w1 = 0;
        return PointToSegment<T,N>( ub0, ua0, ua1, &w0 );
    }
    
    T t[N];
    Vec<T,N>::Init( t, ua0 );
    Vec<T,N>::Sub( t, ub0 );
        
    const T a01 = Vec<T,N>::DotProduct( d0, d1 );
    const T e0 = Vec<T,N>::DotProduct( d0, t );
    const T e1 = Vec<T,N>::DotProduct( d1, t );
    
    const T tmp1 = a00*a11;
    const T tmp2 = a01*a01;
    const T denominator = tmp1 - tmp2;
    const T eps = 10 * DBL_EPSILON * tmp1;
    
    T w0n, w0d = denominator;
    T w1n, w1d = denominator;
    
    if ( denominator <= eps )
    {
        w0n = 0;
        w0d = 1;
        w1n = e1;
        w1d = a11;
    }
    else
    {
        w0n = ( a01*e1 - a11*e0 );
    
        w1n = ( a00*e1 - a01*e0 );
        if ( w0n < 0 )
        {
            w0n = 0;
            w1n = e1;
            w1d = a11;
        }
        else if ( w0n > w0d )
        {
            w0n = w0d;
            w1n = e1 + a01;
            w1d = a11;
        }
    }
    
    if ( w1n < 0 )
    {
        w1n = 0;
        if ( e0 > 0 )
            w0n = 0;
        else if ( e0 < -a00 )
            w0n = w0d;
        else
        {
            w0n = -e0;
            w0d = a00;
        }
    }
    else if ( w1n > w1d )
    {
        w1n = w1d;
        if ( a01 - e0 < 0 )
            w0n = 0;
        else if ( a01 - e0 > a00 )
            w0n = w0d;
        else
        {
            w0n = a01 - e0;
            w0d = a00;
        }
    }
    
    w0 = std::abs( w0n ) <= eps ? 0 : w0n / w0d;
    w1 = std::abs( w1n ) <= eps ? 0 : w1n / w1d;
    
    Vec<T,N>::Mul( d0, w0 );
    Vec<T,N>::Mul( d1, w1 );
    
    T dp[N];
    Vec<T,N>::Init( dp, d0 );
    Vec<T,N>::Sub( dp, d1 );
    Vec<T,N>::Add( dp, t );
    
    return Vec<T,N>::Len( dp );
}

template<typename T>
bool IntersectAxisAndPlane( const Vec3<T>& o, const Vec3<T>& x, const Vec3<T>& y, const Vec3<T>& p, const Vec3<T>& d, Vec3<T>* i_p, T* u, T* v, T* t )
{
    Vec3<T> n;
    Vec3<T>::CrossProduct( x, y, n );
    if ( n.normalize() < DBL_EPSILON )
        return false;

    if ( std::abs( Vec3<T>::DotProduct( n, d ) ) < DBL_EPSILON )
        return false;
    
	const Vec3<T> dr = o - p;
    T axis_t = Vec3<T>::DotProduct( n, dr ) / Vec3<T>::DotProduct( n, d );

    if ( t )
        *t = axis_t;
    
    const Vec3<T> ip = ( d * axis_t ) + p;
    if ( i_p )
        *i_p = ip;
    
    T uv[2];
    Plane::ProjOn( o, x, y, ip, uv[0], uv[1] );

    if ( u )
        *u = uv[0];

    if ( v )
        *v = uv[1];

	return true;
}

template<typename T, size_t N>
T PointToTriangle( const T p[N], const T tr_v0[N], const T tr_v1[N], const T tr_v2[N], T cl_p[N], T* s01, T* t02 )
{
    Vec<T,N> ue0 = tr_v1;
    ue0 -= tr_v0;    
    Vec<T,N> ue1 = tr_v2;
    ue1 -= tr_v0;
    Vec<T,N> up0 = tr_v0;
    up0 -= p;
    
    for ( size_t i = 0; i < N; i++ )
    {
        const double max_val = max_value( std::abs( p[i] ), std::abs( tr_v0[i] ), std::abs( tr_v1[i] ), std::abs( tr_v2[i] ) );
        ue0[i] /= max_val;
        ue1[i] /= max_val;
        up0[i] /= max_val;
    }

    const double a = ue0.dotProduct( ue0 );
    const double b = ue0.dotProduct( ue1 );
    const double c = ue1.dotProduct( ue1 );
    const double d = ue0.dotProduct( up0 );
    const double e = ue1.dotProduct( up0 );
    
    const double det = a*c - b*b;
    double s = b*e - c*d;
    double t = b*d - a*e;
    
    if ( s + t < det )
    {
        if ( s < 0 )
        {
            if ( t < 0 )
            {
                if ( d < 0 )
                {
                    s = clamp<T>( -d/a, 0, 1 );
                    t = 0;
                }
                else
                {
                    s = 0;
                    t = clamp<T>( -e/c, 0, 1 );
                }
            }
            else
            {
                s = 0;
                t = clamp<T>( -e/c, 0, 1 );
            }
        }
        else if ( t < 0 )
        {
            s = clamp<T>( -d/a, 0, 1 );
            t = 0;
        }
        else
        {
            const double invDet = 1 / det;
            s *= invDet;
            t *= invDet;
        }
    }
    else
    {
        if ( s < 0 )
        {
            const double tmp0 = b+d;
            const double tmp1 = c+e;
            if ( tmp1 > tmp0 )
            {
                const double numer = tmp1 - tmp0;
                const double denom = a-2*b+c;
                s = clamp<T>( numer/denom, 0, 1 );
                t = 1-s;
            }
            else
            {
                t = clamp<T>( -e/c, 0, 1 );
                s = 0;
            }
        }
        else if ( t < 0 )
        {
            if ( a+d > b+e )
            {
                const double numer = c+e-b-d;
                const double denom = a-2*b+c;
                s = clamp<T>( numer/denom, 0, 1 );
                t = 1-s;
            }
            else
            {
                s = clamp<T>( -e/c, 0, 1 );
                t = 0;
            }
        }
        else
        {
            const double numer = c+e-b-d;
            const double denom = a-2*b+c;
            s = clamp<T>( numer/denom, 0, 1 );
            t = 1 - s;
        }
    }
    
    Vec<T,N> edge0 = tr_v1;
    edge0 -= tr_v0;
    Vec<T,N> edge1 = tr_v2;
    edge1 -= tr_v0;
    Vec<T, N> nearest_point = ( edge0 * s ) + ( edge1 * t ) + tr_v0;
    if ( cl_p )
        nearest_point.extract( cl_p );    
    
    if ( s01 )
        *s01 = s;
    
    if ( t02 )
        *t02 = t;

    return nearest_point.distance( p );
}

template<typename T>
inline void InitRotationMatrix( const Vec3<T>& axis, const T& angle, Matrix4<T>& matrix )
{
    const T c = std::cos( angle );
    const T s = std::sin( angle );
    const T t = 1 - c;
    const T x = axis[0];
    const T y = axis[1];
    const T z = axis[2];

    matrix.at( 0, 0 ) = t*x*x + c;
    matrix.at( 1, 0 ) = t*x*y + z*s;
    matrix.at( 2, 0 ) = t*x*z - y*s;

    matrix.at( 0, 1 ) = t*x*y - z*s;
    matrix.at( 1, 1 ) = t*y*y + c;
    matrix.at( 2, 1 ) = t*y*z + x*s;

    matrix.at( 0, 2 ) = t*x*z + y*s;
    matrix.at( 1, 2 ) = t*y*z - x*s;
    matrix.at( 2, 2 ) = t*z*z + c;
}

}

