#pragma once

#include <vector>
#include <float.h>
#include "Exception.h"

namespace MA
{

template <typename T>
class Matrix
{
public:
	Matrix()
		: Matrix( 0, 0 )
    {
    }

    Matrix( size_t rows, size_t columns )
    {
        resize( rows, columns );
    }

    void clear()
    {
        resize( 0, 0 );
    }

    void resize( size_t rows, size_t columns )
    {
        m_is = rows;
        m_js = columns;
        m_is_squared = m_is == m_js;
        m_elem_size = m_is * m_js;
        m.resize( m_elem_size );
    }

    void set( const T& v )
    {
        for ( size_t i = 0; i < m_elem_size; i++ )
            m[i] = v;
    }

    void extractColumn( size_t ind, std::vector<T>& line ) const
	{
        if ( ! VERIFY( ind < columnCount() ) )
            return;

        line.resize( m_is );
		for ( size_t i = 0; i < m_is; i++ )
            line[i] = at( i, ind );
	}
    
    void extractRow( size_t ind, std::vector<T>& line ) const
	{
        if ( ! VERIFY( ind < rowCount() ) )
            return;

        line.resize( m_js );
		for ( size_t i = 0; i < m_js; i++ )
            line[i] = at( ind, i );
	}

    void setColumn( size_t ind, const T* arr )
	{
		for ( size_t i = 0; i < m_is; i++ )
            at( i, ind ) = arr[i];
	}

    void setRow( size_t ind, const T* arr )
	{
		for ( size_t i = 0; i < m_js; i++ )
            at( ind, i ) = arr[i];
	}

    bool gauss( Matrix& vector )
	{
		if ( ! VERIFY( m_is == m_js && m_is == vector.m_is ) )
            return false;
		
		if ( ! forward( vector ) )
            return false;
        
        reverse( vector );
        
        return true;
	}

    inline T& at( size_t i, size_t j )
    {
        return m[i + m_is*j];
    }

    inline const T& at( size_t i, size_t j ) const
    {
        return m[i + m_is*j];
    }

    Matrix transpose()
    {
        Matrix matr( m_js, m_is );

        for ( size_t i = 0; i < m_is; i++ )
        {
            for ( size_t j = 0; j < m_js; j++ )
                matr.at( j, i ) = at( i, j );
        }

        return matr;
    }

    Matrix multiply( const Matrix& a )
    {
        Matrix matr( m_is, a.m_js );
        matr.set( 0 );

        if ( m_js != a.m_is )
            return matr;

        for ( size_t j = 0; j < a.m_js; j++ )
        {
            for ( size_t i = 0; i < m_is; i++ )
            {
                for ( size_t k = 0; k < m_js; k++ )
                    matr.at( i, j ) += at( i, k ) * a.at( k, j );
            }
        }

        return matr;
    }

	inline bool isSquared() const
    {
        return m_is_squared;
    }

	inline size_t count( int row0_column1 ) const
    {
        return row0_column1 == 0 ? m_is : m_js;
    }
	
    size_t rowCount() const
    {
        return m_is;
    }

    size_t columnCount() const
    {
        return m_js;
    }

    inline const std::vector<T>& array() const
    {
        return m;
    }

    inline T* data()
    {
        return m.data();
    }

    inline const T* data() const
    {
        return m.data();
    }

private:
    bool forward( Matrix& vector )
	{
		for ( size_t i = 0; i < m_is; i++ )
		{
			size_t max_index = 0;
			T max_value = 0.0;
        
			for ( size_t n = i; n < m_js; n++ )
			{
				if ( std::abs( at( n, i ) ) > max_value )
				{
					max_value = std::abs( at( n, i ) );
					max_index = n;
				}
			}
        
			if ( max_index != i )
				permuteRows( i, max_index, vector );
        
			const T ai1 = at( i, i );
        
			if ( std::abs( ai1 ) > DBL_EPSILON )
			{
				for ( size_t j = 0; j < vector.m_js; j++ )
					vector.at( i, j ) /= ai1;
        
				for ( size_t j = i; j < m_js; j++ )
					at( i, j ) /= ai1;
			}
			else
			{
                vector.set( 0 );
				return false;
			}
        
			for ( size_t ii = i+1; ii < m_is; ii++ )
			{
				const T aii1 = at( ii, i );
				substractRows( i, ii, aii1, vector );
			}
		}
        
		return true;
	}

	void reverse( Matrix& vector )
	{
		for ( size_t i = m_is - 1; i > 0; i-- )
			for ( size_t j = i; j < m_is; j++ )
				for ( size_t k = 0; k < vector.m_js; k++ )
					vector.at( i - 1, k ) = vector.at( i - 1, k ) - vector.at( j, k ) * at( i - 1, j );
	}

	void permuteRows( size_t f, size_t s, Matrix& vector )
	{
		T temp;
        
		for ( size_t i = 0; i < m_js; i++ )
		{
			temp = at( f, i );
			at( f, i ) = at( s, i );
			at( s, i ) = temp;
		}
        
		for ( size_t i = 0; i < vector.m_js; i++ )
		{
			temp = vector.at( f, i );
			vector.at( f, i ) = vector.at( s, i );
			vector.at( s, i ) = temp;
		}
	}

	void substractRows( size_t it, size_t from, T k, Matrix& vector )
	{
		for ( size_t j = it; j < m_js; j++ )
			at( from, j ) = at( from, j ) - ( at( it, j ) * k );
        
		for ( size_t j = 0; j < vector.m_js; j++ )
			vector.at( from, j ) = vector.at( from, j ) - ( vector.at( it, j ) * k );
	}

private:
    size_t m_is; //rows count
    size_t m_js; //column count
    bool m_is_squared;
    size_t m_elem_size;
    std::vector<T> m;
};

using MatrixI = Matrix<int>;
using MatrixD = Matrix<double>;
using MatrixF = Matrix<float>;

}
