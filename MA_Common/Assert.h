#pragma once

#include <assert.h>

namespace MA
{

#ifdef CLION
inline void mart_assert( bool ) {}
#define ASSERT( expression ) MA::mart_assert( bool( expression ) )
#else
#define ASSERT( expression ) assert( expression )
#endif

#define NOTIMPL ASSERT( 0 )
#define NOTIMPL_EXC throw Exception( Exception::S_NotImpl )

inline bool verify( bool expr )
{
#ifdef _DEBUG
    ASSERT( expr );
#endif
    return expr;
}

#define VERIFY( expression ) verify( bool( expression ) )

}
