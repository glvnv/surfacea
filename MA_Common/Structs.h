#pragma once

#include "Color.h"
#include "Vec.h"

namespace MA
{

template <typename T>
struct SharedObject
{
    SharedObject( const T& obj, int u_c ) : use_count( u_c ), object( obj ) {}
    int use_count;
    T object;
};
using SharedPointVD3 = SharedObject<VecD3>;

template <typename T>
struct Segment
{
    T begin;
    T end;
};
using SegmentVecD3 = Segment<VecD3>;
using SegmentVecD2 = Segment<VecD2>;
using SegmentVecF3 = Segment<VecF3>;
using SegmentVecF2 = Segment<VecF2>;

struct PointOn
{
    VecD3Opt  point;    
    VecD2Opt  face_uvpoint;	
    SizeTOpt  face_triangle_index;
    DoubleOpt edge_param;
    SizeTOpt  edge_segment_index;
};

struct Triangle
{
    Triangle() : v{ 0, 0, 0 } {}
    Triangle( size_t v0, size_t v1, size_t v2 ) : v{ v0, v1, v2 } {}

    const size_t& operator[]( size_t i ) const { return v[i]; }
    const size_t& operator[]( int i ) const { return v[i]; }
    size_t& operator[]( size_t i ) { return v[i]; }
    size_t& operator[]( int i ) { return v[i]; }

    size_t v[3];
};

struct PolyRestriction
{
    PolyRestriction()
        : angle_dev( 0 )
        , sag( 0 )
        , sag_boundbox_ratio( 0 )
        , length( 0 )
    {
    }

    PolyRestriction( double angle_dev_, double sag_, double sag_boundbox_ratio_, double length_ )
        : angle_dev( angle_dev_ )
        , sag( sag_ )
        , sag_boundbox_ratio( sag_boundbox_ratio_ )
        , length( length_ )
    {
    }

    double angle_dev;
    double sag;
    double sag_boundbox_ratio;
    double length;
};

struct VisualMode
{
    float edge_thickness = 0;
    float sel_edge_thickness = 0;
    float vertex_size = 0;
    float sel_vertex_size = 0;
};

struct RenderMode
{
    bool triangle_fill = true;
    bool triangle_mesh = false;
    bool edges = true;
    bool vertices = false;
    bool ignore_plane_culling = false;
};

struct ViewMode
{
    double perspective_factor = 1;
    bool perspective_enabled = true;
    bool antialiasing_enabled = true;
    bool scenery_cubemap_enabled = true;
    bool scene_coord_axes = true;
    bool camera_animation = true;
    bool face_front = true;
    bool face_back = true;
    bool hide_twin_edges = true;
};

struct SpecialMode
{
    bool flying_enabled = false;
    bool rentgen_enabled = false;
    bool zebra_enabled = false;
};

struct SelectionMode
{
    bool edge_prop_on_solid = true;
    double ray_picker_catch_tan = 0;
};

struct BackgroundMode
{
    enum Type
    {
        BM_CustomColor = 0,
        BM_SkyTheme = 1
    };

    BackgroundMode() : type( BM_SkyTheme ), color{ 0.5f, 0.5f, 0.5f } {}
    Type type;
    float color[3];
};

struct Colors
{
    MA::Color face;
    MA::Color face_selected;    
    MA::Color face_preselected;
    MA::Color face_triangle_mesh;

    MA::Color edge;
    MA::Color edge_selected;
    MA::Color edge_preselected;

    MA::Color vertex;
    MA::Color vertex_selected;
    MA::Color vertex_preselected;
};

struct Rgba
{
    float r() const { return data[0]; }
    float g() const { return data[1]; }
    float b() const { return data[2]; }
    float a() const { return data[3]; }

    bool operator == ( const Rgba& rgba ) const
    {
        return data[0] == rgba.data[0] && data[1] == rgba.data[1] && data[2] == rgba.data[2] && data[3] == rgba.data[3];
    }

    float data[4];
};

}

