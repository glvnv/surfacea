#pragma once

#include <thread>
#include <future>
#include <fstream>
#include <string>

namespace MA
{

template<typename Container, typename Element>
bool push_back_unique( Container& c, const Element& e )
{
    bool uniq = true;
    for ( auto i = c.begin(), i_end = c.end(); i != i_end; i++ )
    {
        if ( *i != e )
            continue;

        uniq = false;
        break;
    }

    if ( uniq )
    {
        c.push_back( e );
        return true;
    }
    else
        return false;
}

template<typename Container>
bool check_monotonic( const Container& c, bool increase, bool inequality )
{
    if ( c.empty() )
        return true;

    const auto b = c.begin();
    const auto e = c.end();

    auto i = b;
    auto p = i;
    i++;

    for ( ; i != e; i++ )
    {
        if ( increase )
        {
            if ( inequality )
            {
                if ( *i <= *p )
                    return false;
            }
            else
            {
                if ( *i < *p )
                    return false;
            }
        }
        else
        {
            if ( inequality )
            {
                if ( *i >= *p )
                    return false;
            }
            else
            {
                if ( *i > *p )
                    return false;
            }
        }

        p = i;
    }

    return true;
}

inline std::string file_data_string( const std::string& filename )
{
    std::ifstream shader_file( filename );

    if ( ! shader_file )
        throw Exception( Exception::S_General, "file_data_string: file access error" );

    return std::string( ( std::istreambuf_iterator<char>( shader_file )), std::istreambuf_iterator<char>() );
}

class join_threads
{
    std::vector<std::thread>& threads;
public:
    explicit join_threads(std::vector<std::thread>& threads_)
        : threads(threads_)
    {}

    ~join_threads()
    {
        for(unsigned long i=0;i<threads.size();++i)
        {
            if(threads[i].joinable())
                threads[i].join();
        }
    }
};

template <typename Iterator, typename Func>
void parallel_for_each( Iterator first, Iterator last, Func func, unsigned long thread_count_multiplier = 1, unsigned long min_per_thread = 1 )
{
    const long length = long( std::distance( first, last ) );
    if ( ! length )
        return;

    const unsigned long max_threads_elem = ( length + min_per_thread - 1 ) / min_per_thread;
    const unsigned long max_threads_mult = thread_count_multiplier*std::thread::hardware_concurrency();
    const unsigned long num_threads = std::min( max_threads_mult != 0 ? max_threads_mult : 2, max_threads_elem );

    const unsigned long block_size = length / num_threads;

    std::vector<std::future<void>> futures( num_threads - 1 );
    std::vector<std::thread> threads( num_threads - 1 );
    join_threads joiner( threads );

    Iterator block_start = first;
    for ( unsigned long i = 0; i < num_threads - 1; i++ )
    {
        Iterator block_end = block_start;
        std::advance( block_end, block_size );
        std::packaged_task<void( void )> task (
        [block_start, block_end, func]()
        {
            std::for_each( block_start, block_end, func );
        });

        futures[i] = task.get_future();
        threads[i] = std::thread( std::move( task ) );
        block_start = block_end;
    }

    std::for_each( block_start, last, func );

    for ( unsigned long i = 0; i < num_threads - 1; i++ )
        futures[i].get();
}

inline int8_t endian_reverse( int8_t x )
{
    return x;
}

inline int16_t endian_reverse( int16_t x )
{
    return ( static_cast<uint16_t>( x ) << 8 ) | ( static_cast<uint16_t>(x) >> 8 );
}

inline int32_t endian_reverse( int32_t x )
{
    uint32_t step16;
    step16 = static_cast<uint32_t>( x ) << 16 | static_cast<uint32_t>( x ) >> 16;
    return ( ( static_cast<uint32_t>( step16 ) << 8 ) & 0xff00ff00 ) | ( ( static_cast<uint32_t>( step16 ) >> 8 ) & 0x00ff00ff );
}

inline int64_t endian_reverse( int64_t x )
{
    uint64_t step32, step16;
    step32 = static_cast<uint64_t>(x) << 32 | static_cast<uint64_t>(x) >> 32;
    step16 = ( step32 & 0x0000FFFF0000FFFFULL ) << 16 | ( step32 & 0xFFFF0000FFFF0000ULL ) >> 16;
    return static_cast<int64_t>( ( step16 & 0x00FF00FF00FF00FFULL ) << 8 | ( step16 & 0xFF00FF00FF00FF00ULL ) >> 8 );
}

inline uint8_t endian_reverse( uint8_t x )
{
    return x;
}

inline uint16_t endian_reverse( uint16_t x )
{
    return ( x << 8 ) | ( x >> 8 );
}

inline uint32_t endian_reverse( uint32_t x )
{
    uint32_t step16;
    step16 = x << 16 | x >> 16;
    return ( ( step16 << 8 ) & 0xff00ff00 ) | ( ( step16 >> 8 ) & 0x00ff00ff );
}

inline uint64_t endian_reverse( uint64_t x )
{
   uint64_t step32, step16;
   step32 = x << 32 | x >> 32;
   step16 = ( step32 & 0x0000FFFF0000FFFFULL ) << 16 | ( step32 & 0xFFFF0000FFFF0000ULL ) >> 16;
   return ( step16 & 0x00FF00FF00FF00FFULL ) << 8 | ( step16 & 0xFF00FF00FF00FF00ULL ) >> 8;
}

inline float endian_reverse( float x )
{
    float res;
    char *floatToConvert = ( char* ) & x;
    char *returnFloat = ( char* ) & res;

    // swap the bytes into a temporary buffer
    returnFloat[0] = floatToConvert[3];
    returnFloat[1] = floatToConvert[2];
    returnFloat[2] = floatToConvert[1];
    returnFloat[3] = floatToConvert[0];

    return res;
}

inline double endian_reverse( double x )
{
    double res;
    char *doubleToConvert = ( char* ) & x;
    char *return_double = ( char* ) & res;

    // swap the bytes into a temporary buffer
    return_double[0] = doubleToConvert[7];
    return_double[1] = doubleToConvert[6];
    return_double[2] = doubleToConvert[5];
    return_double[3] = doubleToConvert[4];
    return_double[4] = doubleToConvert[3];
    return_double[5] = doubleToConvert[2];
    return_double[6] = doubleToConvert[1];
    return_double[7] = doubleToConvert[0];

    return res;
}

}
