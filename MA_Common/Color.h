#pragma once

#include <algorithm>

namespace MA
{

const float CHROME_DEFAULT = 0.6f;
const float SHININESS_DEFAULT = 25;

struct Color
{
    Color()
    {
        ambient[0] = 0;
        ambient[1] = 0;
        ambient[2] = 0;

        diffuse[0] = 0;
        diffuse[1] = 0;
        diffuse[2] = 0;

        specular[0] = 0;
        specular[1] = 0;
        specular[2] = 0;

        shininess = 0;
        alpha = 0;
        chrome = 0;
    }

    Color( const Color& ) = default;

    Color( const Color& c0, const Color& c1, float w0 = 0.5f, float w1 = 0.5f )
    {
        set( c0, c1, w0, w1 );
    }

    Color( float r, float g, float b, float a = 0, float c = CHROME_DEFAULT )
    {
        set( r, g, b, a, c );
    }

    Color( float ambient_r,  float ambient_g,  float ambient_b,
           float diffuse_r,  float diffuse_g,  float diffuse_b,
           float specular_r, float specular_g, float specular_b,
           float shininess_, float alpha_, float chrome_ )
    {
        ambient[0] = ambient_r;
        ambient[1] = ambient_g;
        ambient[2] = ambient_b;

        diffuse[0] = diffuse_r;
        diffuse[1] = diffuse_g;
        diffuse[2] = diffuse_b;

        specular[0] = specular_r;
        specular[1] = specular_g;
        specular[2] = specular_b;

        shininess = shininess_;
        alpha = alpha_;
        chrome = chrome_;
    }

    Color( const float* ambient_, const float* diffuse_, const float* specular_, float shininess_, float alpha_, float chrome_ )
    {
        set( ambient_, diffuse_, specular_, shininess_, alpha_, chrome_ );
    }

    Color( const float* color_, float ambient_, float diffuse_, float specular_, float shininess_, float alpha_ = 0, float chrome_ = CHROME_DEFAULT )
    {
        ambient[0] = color_[0] * ambient_;
        ambient[1] = color_[1] * ambient_;
        ambient[2] = color_[2] * ambient_;

        diffuse[0] = color_[0] * diffuse_;
        diffuse[1] = color_[1] * diffuse_;
        diffuse[2] = color_[2] * diffuse_;

        specular[0] = color_[0] * specular_;
        specular[1] = color_[1] * specular_;
        specular[2] = color_[2] * specular_;

        shininess = shininess_;
        alpha = alpha_;
        chrome = chrome_;
    }

    void set( const float* ambient_, const float* diffuse_, const float* specular_, float shininess_, float alpha_, float chrome_ )
    {
        if ( ambient_ )
            std::copy( ambient_, ambient_ + 3, ambient );

        if ( diffuse_ )
            std::copy( diffuse_, diffuse_ + 3, diffuse );

        if ( specular_ )
            std::copy( specular_, specular_ + 3, specular );

        shininess = shininess_;
        alpha = alpha_;
        chrome = chrome_;
    }

    void set( float r, float g, float b, float a = 0, float c = CHROME_DEFAULT )
    {
        ambient[0] = 0.6f*r;
        ambient[1] = 0.6f*g;
        ambient[2] = 0.6f*b;
    
        diffuse[0] = 0.3f*r;
        diffuse[1] = 0.3f*g;
        diffuse[2] = 0.3f*b;
    
        specular[0] = r;
        specular[1] = g;
        specular[2] = b;
    
        shininess = SHININESS_DEFAULT;
        alpha = a;
        chrome = c;
    }

    void set( const Color& c0, const Color& c1, float w0 = 0.5f, float w1 = 0.5f )
    {
        ambient[0]  = w0 * c0.ambient[0]  + w1 * c1.ambient[0];
        ambient[1]  = w0 * c0.ambient[1]  + w1 * c1.ambient[1];
        ambient[2]  = w0 * c0.ambient[2]  + w1 * c1.ambient[2];

        diffuse[0]  = w0 * c0.diffuse[0]  + w1 * c1.diffuse[0];
        diffuse[1]  = w0 * c0.diffuse[1]  + w1 * c1.diffuse[1];
        diffuse[2]  = w0 * c0.diffuse[2]  + w1 * c1.diffuse[2];

        specular[0] = w0 * c0.specular[0] + w1 * c1.specular[0];
        specular[1] = w0 * c0.specular[1] + w1 * c1.specular[1];
        specular[2] = w0 * c0.specular[2] + w1 * c1.specular[2];

        shininess = w0 * c0.shininess + w1 * c1.shininess;
        alpha = w0 *c0.alpha + w1 * c1.alpha;
        chrome = w0 *c0.chrome + w1 * c1.chrome;
    }

    void elementary( float& r, float& g, float& b )
    {
        r = ( ambient[0] + diffuse[0] + specular[0] ) / 3;
        g = ( ambient[1] + diffuse[1] + specular[1] ) / 3;
        b = ( ambient[2] + diffuse[2] + specular[2] ) / 3;
    }

    bool equal ( const Color& color ) const
    {
        if ( shininess != color.shininess )
            return false;

        if ( alpha != color.alpha )
            return false;

        if ( chrome != color.chrome )
            return false;

        if ( ambient[0] != color.ambient[0] || ambient[1] != color.ambient[1] || ambient[2] != color.ambient[2] )
            return false;

        if ( diffuse[0] != color.diffuse[0] || diffuse[1] != color.diffuse[1] || diffuse[2] != color.diffuse[2] )
            return false;

        if ( specular[0] != color.specular[0] || specular[1] != color.specular[1] || specular[2] != color.specular[2] )
            return false;

        return true;
    }

    bool operator == ( const Color& color ) const
    {
        return equal( color );
    }

    bool operator != ( const Color& color ) const
    {
        return ! equal( color );
    }

    float ambient[3];
    float diffuse[3];
    float specular[3];
    float shininess;
    float alpha;
    float chrome;
};

}

