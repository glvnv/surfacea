#pragma once

#include "Vec.h"
#include "VarOpt.h"
#include "Matrix4.h"

namespace MA
{
	
template <typename T>
class CoordSystem
{
public:
    CoordSystem() {}
    CoordSystem( const Vec3<T>& o, const Vec3<T>& x, const Vec3<T>& y, const Vec3<T>& z )
    {
        set( o, x, y, z );
    }

    const Vec3<T>& o() const { return m_oxyz[0]; }
    const Vec3<T>& x() const { return m_oxyz[1]; }
    const Vec3<T>& y() const { return m_oxyz[2]; }
    const Vec3<T>& z() const { return m_oxyz[3]; }

    void setO( const Vec3<T>& o ) { m_oxyz[0] = o; }
    void setX( const Vec3<T>& x ) { m_oxyz[1] = x; }
    void setY( const Vec3<T>& y ) { m_oxyz[2] = y; }
    void setZ( const Vec3<T>& z ) { m_oxyz[3] = z; }

    void set( const Vec3<T>& o, const Vec3<T>& x, const Vec3<T>& y, const Vec3<T>& z )
    {
        m_oxyz[0] = o;
        m_oxyz[1] = x;
        m_oxyz[2] = y;
        m_oxyz[3] = z;

        const T xlen = m_oxyz[1].normalize();
        const T ylen = m_oxyz[2].normalize();
        const T zlen = m_oxyz[3].normalize();

        if ( xlen == 0 && ylen != 0 && zlen != 0 )
        {
            m_oxyz[1] = m_oxyz[2].crossProduct( m_oxyz[3] );
            m_oxyz[1].normalize();
        }
        else if ( xlen != 0 && ylen == 0 && zlen != 0 )
        {
            m_oxyz[2] = m_oxyz[3].crossProduct( m_oxyz[1] );
            m_oxyz[2].normalize();
        }
        else if ( xlen != 0 && ylen != 0 && zlen == 0 )
        {
            m_oxyz[3] = m_oxyz[1].crossProduct( m_oxyz[2] );
            m_oxyz[3].normalize();
        }
    }

    void initByX( const Vec3<T>& o, const Vec3<T>& x )
    {
        m_oxyz[0] = o;
        m_oxyz[1] = x;
        m_oxyz[1].normalize();
        Vec3<T> z( 0, 0, 1 );

        if ( m_oxyz[1] * z > std::sqrt( 2 ) / 2 )
            z.init( 0, 1, 0 );
        
        m_oxyz[2] = z.crossProduct( x );
        m_oxyz[2].normalize();

        m_oxyz[3] = m_oxyz[1].crossProduct( m_oxyz[2] );
    }

    void transform( const Matrix4<T>& transform )
    {
        Matrix4<T> csm;
        csm.at( 0, 0 ) = m_oxyz[1][0]; csm.at( 0, 1 ) = m_oxyz[2][0]; csm.at( 0, 2 ) = m_oxyz[3][0]; csm.at( 0, 3 ) = m_oxyz[0][0];
        csm.at( 1, 0 ) = m_oxyz[1][1]; csm.at( 1, 1 ) = m_oxyz[2][1]; csm.at( 1, 2 ) = m_oxyz[3][1]; csm.at( 1, 3 ) = m_oxyz[0][1];
        csm.at( 2, 0 ) = m_oxyz[1][2]; csm.at( 2, 1 ) = m_oxyz[2][2]; csm.at( 2, 2 ) = m_oxyz[3][2]; csm.at( 2, 3 ) = m_oxyz[0][2];

        Matrix4<T> new_csm = transform * csm;
        m_oxyz[0][0] = new_csm.at( 0, 3 ); m_oxyz[1][0] = new_csm.at( 0, 0 ); m_oxyz[2][0] = new_csm.at( 0, 1 ); m_oxyz[3][0] = new_csm.at( 0, 2 );
        m_oxyz[0][1] = new_csm.at( 1, 3 ); m_oxyz[1][1] = new_csm.at( 1, 0 ); m_oxyz[2][1] = new_csm.at( 1, 1 ); m_oxyz[3][1] = new_csm.at( 1, 2 );
        m_oxyz[0][2] = new_csm.at( 2, 3 ); m_oxyz[1][2] = new_csm.at( 2, 0 ); m_oxyz[2][2] = new_csm.at( 2, 1 ); m_oxyz[3][2] = new_csm.at( 2, 2 );
    }

private:
    Vec3<T> m_oxyz[4];
};

using CoordSystemD = CoordSystem<double>;
using CoordSystemF = CoordSystem<float>;

using CoordSystemDOpt = VarOpt<CoordSystemD>;
using CoordSystemFOpt = VarOpt<CoordSystemF>;

}

