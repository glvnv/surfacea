#pragma once

#include "Exception.h"
#include <algorithm>

namespace MA
{

template <typename T, size_t N>
class StackVector
{
public:
    typedef T* iterator;
    typedef const T* const_iterator;
    typedef T& ref;
    typedef const T& const_ref;

    StackVector()
        : m_count ( 0 )
    {
    }

    StackVector( const StackVector<T, N>& sv )
    {
        m_count = sv.m_count;
        for ( size_t i = 0; i < m_count; i++ )
            m_data[i] = sv.m_data[i];
    }

    StackVector( size_t size )
    {
        resize( size );
    }

    bool empty() const
    {
        return m_count == 0;
    }

    static size_t Capacity()
    {
        return N;
    }

    size_t size() const
    {
        return m_count;
    }

    void clear()
    {
        std::for_each( m_data, m_data + m_count, []( T& val ){ val = T(); } );
        m_count = 0;
    }

    void resize( size_t new_size )
    {
        if ( new_size > N )
            throw Exception( Exception::S_General, "StackVector::resize() : new_size > size" );

        if ( new_size < m_count )
            std::for_each ( m_data + new_size, m_data + m_count, []( T& val ){ val = T(); } );

        m_count = new_size;
    }

    void push_back( const_ref val )
    {
        if ( m_count == N )
            throw Exception( Exception::S_General, "StackVector::push_back() : new_size > size" );

        m_data[m_count] = val;
        m_count++;
    }

    void pop_back()
    {
        if ( m_count == 0 )
            throw Exception( Exception::S_General, "StackVector::push_back() : new_size > size" );

        m_count--;
        m_data[m_count] = T();
    }

    const_ref operator[] ( size_t pos ) const
    {
        if ( pos >= m_count )
            throw Exception( Exception::S_General, "StackVector::operator[]" );

        return m_data[pos];
    }

    ref operator[] ( size_t pos )
    {
        if ( pos >= m_count )
            throw Exception( Exception::S_General, "StackVector::operator[]" );

        return m_data[pos];
    }

    const T* data() const
    {
        return m_data;
    }

    T* data()
    {
        return m_data;
    }

    iterator begin()
    {
        return m_data;
    }

    const_iterator begin() const
    {
        return m_data;
    }

    iterator end()
    {
        return m_data + m_count;
    }

    const_iterator end() const
    {
        return m_data + m_count;
    }

    ref front()
    {
        return *m_data;
    }

    const_ref front() const
    {
        return *m_data;
    }

    ref back()
    {
        if ( m_count < 1 )
            throw Exception( Exception::S_General, "StackVector::back()" );

        return m_data[m_count-1];
    }

    const_ref back() const
    {
        if ( m_count < 1 )
            throw Exception( Exception::S_General, "StackVector::back()" );

        return m_data[m_count-1];
    }

private:
    T m_data[N];
    size_t m_count;
};

}

