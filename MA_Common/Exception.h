#pragma once

#include <memory>
#include "Assert.h"
#include "String.h"
#include "Log.h"

namespace MA
{

class Exception
{
public:
    enum ExceptionType
    {        
        T_General,
        T_Core,
        T_Engine,        
        T_Scene,        
        T_GUI
    };

    enum StatusType
    {
        S_General = 0,
        S_NotImpl,
        S_NotInitialized,
        S_NullPtr,
        S_IncorrectArgument,
        S_External
    };

    Exception( StatusType status ) : m_status( status ), m_str( "unknown_exception" ) { LOG( std::string( "Exception: " ) + m_str.to_std_string() ); ASSERT ( 0 ); }
    Exception( StatusType status, const String& str ) : m_status( status ), m_str( str ) { LOG( std::string( "Exception: " ) + m_str.to_std_string() ); ASSERT ( 0 ); }
    virtual ~Exception() {}

    virtual ExceptionType type() const { return T_General; }
    StatusType status() const { return m_status; }
    const String& str() const { return m_str; }

private:
    StatusType m_status;
    String m_str;
};

}
