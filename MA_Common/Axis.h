#pragma once

#include "VarOpt.h"
#include "Vec.h"

namespace MA
{
template <typename T>
class Axis
{
public:
    Axis() {}
    Axis( const T& point, const T& dir ) : m_point( point ), m_dir( dir ) {}

    void set( const T& point, const T& dir )
    {
        m_point = point;
        m_dir = dir;
    }

    const T& pos() const
    {
        return m_point;
    }

    const T& dir() const
    {
        return m_dir;
    }

    T& setPos()
    {
        return m_point;
    }

    T& setDir()
    {
        return m_dir;
    }

    void setPos( const T& pos )
    {
        m_point = pos;
    }

    void setDir( const T& dir )
    {
        m_dir = dir;
    }

private:
    T m_point;
    T m_dir;
};

using AxisD3 = Axis<VecD3>;
using AxisD2 = Axis<VecD2>;
using AxisD3Opt = VarOpt<AxisD3>;

}

