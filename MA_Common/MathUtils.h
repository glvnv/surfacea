#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include <cfloat>
#include "Vec.h"
#include "Matrix4.h"

namespace MA
{
const double MC_ONE_PLUS = 1. + 10*DBL_EPSILON;
const double MC_ONE_MINUS = 1. - 10*DBL_EPSILON;
const double MC_MAX = 1.e10;

template<typename T>
inline T squared( T a ) { return a * a; }

template<typename T>
inline T rad_to_deg( T a ) { return ( 180 * a ) / M_PI; }

template<typename T>
inline T deg_to_rad( T a ) { return ( M_PI * a ) / 180; }

template<typename T>
inline T clamp( const T& a, const T& min_a, const T& max_a )
{
    if ( min_a < max_a )
    {
        if ( a > min_a )
        {
            if ( a < max_a )
                return a;
            else
                return max_a;
        }
        else
            return min_a;
    }
    else
    {
        if ( a > max_a )
        {
            if ( a < min_a )
                return a;
            else
                return min_a;
        }
        else
            return max_a;
    }
}

template<typename T>
inline T order( T a )
{
    T ord = 1;

    while ( ord <= a / 5 )
        ord *= 10;

    while ( ord > 2*a )
        ord /= 10;
        
    return ord;
}

template<typename T>
inline T asin_safe( T a )
{
    if ( a > 1 )
        a = 1;
    else if ( a < -1 )
        a = -1;

    return std::asin( a );
}

template<typename T>
inline T acos_safe( T a )
{
    if ( a > 1 )
        a = 1;
    else if ( a < -1 )
        a = -1;

    return std::acos( a );
}

template<typename T>
inline T sqrt_safe( T a )
{
    return a > 0 ? std::sqrt( a ) : 0;
}

template<typename T>
inline T get_angle( T y, T x )
{
    T angle = T( std::atan2( y, x ) );
    if ( angle < 0 )
        angle += T( 2*M_PI );

    return angle;
}

template<typename T, size_t N>
T get_angle( const Vec<T,N>& dir, const Vec<T,N>& ref_udir, const Vec<T,N>& rot_udir )
{
    const T cos1 = dir.dotProduct( ref_udir );
    const T cos2 = dir.dotProduct( rot_udir );
    return get_angle( cos2, cos1 );
}

template<typename T, size_t N>
void rotate( const Vec<T,N>& x_dir, const Vec<T,N>& y_dir, double t, Vec<T,N>& p )
{
    p = ( x_dir * std::cos( t ) ) + ( y_dir * std::sin( t ) );
}

template<typename T>
inline const T& max_value( const T& a, const T& b )
{
    return a > b ? a : b;
}

template<typename T>
inline const T& min_value( const T& a, const T& b )
{
    return a < b ? a : b;
}

template<typename T>
inline const T& max_value( const T& a, const T& b, const T& c )
{
    if ( a > b && a > c )
        return a;
    if ( b > a && b > c )
        return b;
    
    return c;
}

template<typename T>
inline const T& max_value( const T& a, const T& b, const T& c, const T& d )
{
    return max_value( max_value( a, b ), max_value( c, d ) );
}

template<typename T>
inline const T& min_value( const T& a, const T& b, const T& c )
{
    if ( a < b && a < c )
        return a;
    if ( b < a && b < c )
        return b;
    
    return c;
}

template<typename T>
inline const T& min_value( const T& a, const T& b, const T& c, const T& d )
{
    return min_value( min_value( a, b ), min_value( c, d ) );
}

template<typename T, typename V>
void reparam_interval( const T& base_start, const T& base_end, const V& base, const T& t_start, const T& t_end, V& t_params )
{
    if ( ! VERIFY( base.size() > 1 && &base != &t_params ) )
        return;

    t_params.resize( base.size() );
    t_params.front() = t_start;
    t_params.back() = t_end;
    for ( size_t i = 1; i < t_params.size() - 1; i++ )
        t_params[i] = t_params[i-1] + ( ( t_end - t_start ) * ( base[i] - base[i-1] ) ) / ( base_end - base_start );
}

}
