#pragma once

#include "Exception.h"
#include "VarOpt.h"
#include "MathUtils.h"
#include <float.h>
#include "Vec.h"

namespace MA
{

template<typename T, size_t N> class Box;

template <typename T>
void Box2Init( Box<T, 2>& b, const T& umin, const T& vmin, const T& umax, const T& vmax )
{
    b.set( 0, umin );
    b.set( 1, vmin );
    b.set( 2, umax );
    b.set( 3, vmax );
}

template <typename T>
void Box3Init( Box<T, 3>& b, const T& xmin, const T& ymin, const T& zmin, const T& xmax, const T& ymax, const T& zmax )
{
    b.set( 0, xmin );
    b.set( 1, ymin );
    b.set( 2, zmin );
    b.set( 3, xmax );
    b.set( 4, ymax );
    b.set( 5, zmax );
}

template<typename R, typename A, size_t N>
void Init( const Box<A,N>& a, Box<R,N>& b )
{
    for ( size_t i = 0; i < 2*N; i++ )
        b.set( i, R( a[i] ) );
}

template <typename T, size_t N>
class Box
{
public:
    Box()
    {
        clear();
    }

    Box( const Box<T,N>& box )
    {
        set( box );
    }

    Box( const Vec<T,N>& uv_min, const Vec<T,N>& uv_max )
    {
        set( uv_min, uv_max );
    }

    bool empty() const
    {
        return m_is_empty;
    }

    void clear()
    {
        m_is_empty = true;
        for ( size_t i = 0; i < 2*N; i++ )
            m_v[i] = 0;
    }

    void set( size_t i, const T& v )
    {
        m_is_empty = false;
        m_v[i] = v;
    }

    void set( const Box<T,N>& box )
    {
        m_is_empty = box.m_is_empty;
        for ( size_t i = 0; i < 2*N; i++ )
            m_v[i] = box.m_v[i];
    }

    void set( const Vec<T,N>& uv_min, const Vec<T,N>& uv_max )
    {
        m_is_empty = false;
        uv_min.extract( m_v );
        uv_max.extract( m_v + N );
    }

    bool isPointInBox( const T* point ) const
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        for ( size_t i = 0; i < N; i++ )
        {
            if ( point[i] < m_v[i] || point[i] > m_v[i+N] )
                return false;
        }

        return true;
    }

    void center( T* center ) const
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        for ( size_t i = 0; i < N; i++ )
            center[i] = ( m_v[i] + m_v[i+N] ) / 2;
    }

    Vec<T,N> center() const
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        Vec<T,N> c;
        center( c );
        return c;
    }

    T diagonalSquared() const
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        Vec<T,N> v0( m_v );
        Vec<T,N> v1( m_v + 3 );
        return v0.distanceSquared( v1 );
    }

    T diagonal() const
    {
        return std::sqrt( diagonalSquared() );
    }

    T distanceSquaredToPoint( const T* point )
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        T d2 = 0;
        for ( size_t i = 0; i < N; i++ )
        {
            if ( point[i] < m_v[i] )
                d2 += squared( m_v[i] - point[i] );
            else if ( point[i] > m_v[i+N] )
                d2 += squared( point[i] - m_v[i+N] );
        }

        return d2;
    }

    void move( const Vec<T,N>& vec )
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "appendPoint: Box is not valid" );

        for ( size_t i = 0; i < 2*N; i++ )
            m_v[i] += vec[i%N];
    }

    void appendPoint( const T* point )
    {
        if ( m_is_empty )
        {
            for ( size_t i = 0; i < 2*N; i++ )
                m_v[i] = point[i%N];

            m_is_empty = false;
        }
        else
        {
            for ( size_t i = 0; i < N; i++ )
                m_v[i] = std::min( m_v[i], point[i] );

            for ( size_t i = N; i < 2*N; i++ )
                m_v[i] = std::max( m_v[i], point[i-N] );
        }
    }

    void appendBox( const Box<T,N>& box )
    {
        if ( box.empty() )
            return;

        if ( m_is_empty )
        {
            set( box );
        }
        else
        {
            m_is_empty = false;
            for ( size_t i = 0; i < N; i++ )
                m_v[i] = std::min( m_v[i], box.m_v[i] );

            for ( size_t i = N; i < 2*N; i++ )
                m_v[i] = std::max( m_v[i], box.m_v[i] );
        }
    }

    bool isIntersected( const Box<T,N>& box, const T& tolerance = 0 ) const
    {
        if ( m_is_empty || box.m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );
        
        for ( size_t i = 0; i < N; i++ )
        {
            if ( box.m_v[i+N] < m_v[i] - tolerance || m_v[i+N] < box.m_v[i] - tolerance )
                return false;
        }
        
        return true;
    }

    bool isContaining( const Box<T,N>& box ) const
    {
        if ( m_is_empty || box.m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );
        
        for ( size_t i = 0; i < N; i++ )
        {
            if ( box.m_v[i] < m_v[i] || m_v[i+N] < box.m_v[i+N] )
                return false;
        }
        
        return true;
    }

    const T* data() const
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        return m_v;
    }

    T* data()
    {
        if ( m_is_empty )
            throw Exception( Exception::S_NotInitialized, "Box is not valid" );

        return m_v;
    }

    const T& operator [] ( size_t i ) const
    {
        if ( m_is_empty || i >= 2*N )
            throw Exception( Exception::S_General, "Box is not valid" );

        return m_v[i];
    }

private:
    bool m_is_empty;
    T m_v[2*N];
};

using BoxD3 = Box<double, 3>;
using BoxF3 = Box<float, 3>;

using BoxD2 = Box<double, 2>;
using BoxF2 = Box<float, 2>;

using IntervalD = Box<double, 1>;
using IntervalF = Box<float, 1>;

using BoxD3Opt = VarOpt<BoxD3>;
using BoxF3Opt = VarOpt<BoxF3>;

using BoxD2Opt = VarOpt<BoxD2>;
using BoxF2Opt = VarOpt<BoxF2>;

using IntervalDOpt = VarOpt<IntervalD>;
using IntervalFOpt = VarOpt<IntervalF>;

template <typename T>
void Box3Vertices( const Box<T, 3>& box, Vec<T, 3> points[8] )
{
    points[0] = Vec3Init( box[0], box[1], box[2] );
    points[1] = Vec3Init( box[3], box[1], box[2] );
    points[2] = Vec3Init( box[3], box[4], box[2] );
    points[3] = Vec3Init( box[0], box[4], box[2] );
    points[4] = Vec3Init( box[0], box[1], box[5] );
    points[5] = Vec3Init( box[3], box[1], box[5] );
    points[6] = Vec3Init( box[3], box[4], box[5] );
    points[7] = Vec3Init( box[0], box[4], box[5] );
}

// 0 - xmin, 1 - ymin, 2 - zmin, 3 - xmax, 4 - ymax, 5 - zmax

template <typename T>
void Rect3Vertices( const Box<T, 3>& box, int side, Vec<T, 3> points[4] )
{
    switch ( side )
    {
    case 0:
        points[0] = Vec3Init( box[0], box[4], box[2] );
        points[1] = Vec3Init( box[0], box[1], box[2] );
        points[2] = Vec3Init( box[0], box[1], box[5] );
        points[3] = Vec3Init( box[0], box[4], box[5] );
        break;
    case 1:
        points[0] = Vec3Init( box[0], box[1], box[2] );
        points[1] = Vec3Init( box[3], box[1], box[2] );
        points[2] = Vec3Init( box[3], box[1], box[5] );
        points[3] = Vec3Init( box[0], box[1], box[5] );
        break;
    case 2:
        points[0] = Vec3Init( box[3], box[1], box[2] );
        points[1] = Vec3Init( box[0], box[1], box[2] );
        points[2] = Vec3Init( box[0], box[4], box[2] );
        points[3] = Vec3Init( box[3], box[4], box[2] );
        break;
    case 3:
        points[0] = Vec3Init( box[3], box[4], box[2] );
        points[1] = Vec3Init( box[3], box[4], box[5] );
        points[2] = Vec3Init( box[3], box[1], box[5] );
        points[3] = Vec3Init( box[3], box[1], box[2] );
        break;
    case 4:
        points[0] = Vec3Init( box[0], box[4], box[2] );
        points[1] = Vec3Init( box[0], box[4], box[5] );
        points[2] = Vec3Init( box[3], box[4], box[5] );
        points[3] = Vec3Init( box[3], box[4], box[2] );
        break;
    case 5:
        points[0] = Vec3Init( box[3], box[1], box[5] );
        points[1] = Vec3Init( box[3], box[4], box[5] );
        points[2] = Vec3Init( box[0], box[4], box[5] );
        points[3] = Vec3Init( box[0], box[1], box[5] );
        break;
    default:
        throw Exception( Exception::S_IncorrectArgument, "Rect3Vertices" );
    }
}

}

